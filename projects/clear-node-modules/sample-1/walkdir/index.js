
const fs = require('fs');
const path = require('path');

const walk = (dir, filter, fn) => {
  dir = path.join(dir);
  console.log(`${dir}`);
  fs.readdir(dir, (err, items) => {
    if (err) throw err;
    items.forEach(item => {
      const fullpath = path.join(dir, item);
      fs.stat(fullpath, (err, stat) => {
        if (err) console.error(`STAT: ${fullpath}: ${err.message}`);
        if (filter(item, fullpath, stat))
          fn(item, fullpath, stat);
        else if (stat.isDirectory())
          walk(fullpath, filter, fn);
      });
    });
  });
};

module.exports = walk;
