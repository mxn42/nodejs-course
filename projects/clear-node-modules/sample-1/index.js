
const deldir = require('./deldir');
const walkdir = require('./walkdir');

walkdir(
    process.argv[2],
    (name, fullpath, stat) => stat.isDirectory() && name === 'node_modules',
    (name, fullpath, stat) => deldir(fullpath, (err) => {
      if (err) console.error(err);
      else console.info(`DELETED: ${fullpath}`);
    })
);
