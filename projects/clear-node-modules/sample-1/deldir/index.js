
const fs = require('fs');
const path = require('path');

const deldir = (dir, callback) => {
  fs.readdir(dir, (err, items) => {
    if (err) {
      callback(err);
      return;
    }
    let counters = items.length;
    const checkFinish = () => {
      counters--;
      if (counters < 0)
        fs.rmdir(dir, callback);
    };
    checkFinish();
    items.forEach(item => {
      const itemPath = path.join(dir, item);
      fs.stat(itemPath, (err, stat) => {
        if (err)
          console.error(`ERROR STAT: ${itemPath}`);
        else if (stat.isDirectory())
          deldir(itemPath, err => {
            if (err) console.error(`ERROR DIR DELETE: ${itemPath}`);
            checkFinish();
          });
        else {
          fs.unlink(itemPath, err => {
            if (err) console.error(`ERROR FILE DELETE: ${itemPath}`);
            checkFinish();
          });
        }
      });
    });
  });
};


module.exports = deldir;
