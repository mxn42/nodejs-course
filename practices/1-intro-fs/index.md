
# Practice 1: Node.sj, modules, file system

 - Первое приложение
 - Интерфейс командной строки
 - Вычисления
 - Прочитать и записать файлы
 - Модули, package.json, npm
 - Просмотреть все файлы в папке
 - Удалить папку, удалить все папки node_modules
 - Запросить картинки с веб-сайта и сохранить
 
[Lecture 1](../../lectures/1)

## Pre-task

 - Установить ноду, проверить версию.
 - Написать скрипт с выводом `Hello World` и запустить в Node.js.
 - Вычислить 20! 
 - Подключить модуль `fs`. Прочитать имя из файла `name.txt`, вывести в файл `hello-name.txt` приветствие полученного имени.

## Tasks

[tasks.md](./tasks.md)

## Темы

 - Параметры запуска (process.argv)
 - Файловые операции
   - чтение и запись файлов
   - список файлов в папке, рекурсивный просмотр каталога
   - свойста файлов
   - удаление каталога
   - чтение и запись JSON файла
   - path
 - Модули
   - установка
   - версионность semver.org
   - подключение
   - поиск   


**Globals**

- Globals <https://nodejs.org/docs/latest/api/globals.html>

 - `__dirname`
 - `__filename`
 - `exports`
 - `module`
 - `require()`
 - `console`
 - `setImmediate`, `clearImmediate`


**process**

- process <https://nodejs.org/docs/latest/api/process.html#process_process_argv>

[.argv](https://nodejs.org/docs/latest/api/process.html#process_process_argv)
```
process.argv[0] = node
process.argv[1] = %path%/index.js
process.argv[2] = Mark
process.argv[3] = Twain
```

Если нужно сложный интерфейс командной строки, то парсинг данных может оказаться дорогой операцией. Готовые библиотеки: 
`Minimist`, `Commander.js`, `Meow`, `Yargs`, `Vorpal.js`, `Promptly`, `Co-Prompt`.

**console**

```javascript
console.log('Hello, World!');
console.error('Oops, something\'s gone wrong.');
console.time('Mark Twain');
console.timeEnd('Mark Twain');
```

**fs, path**

 - fs <https://nodejs.org/docs/latest/api/fs.html>
 - path <https://nodejs.org/docs/latest/api/path.html>


**Aync: Callback, Promise, Async-Await**

Событийная модель Node.js.

**packages.json, node_modules**

Подключение внешних модулей.

