
const factorial = N => {
  let f = 1;
  let i = 1;
  while (i <= N) {
    f *= i;
    i += 1;
  }
  return f;
};

const N = process.argv[0];
console.time(`${N}!`);
console.log(factorial(N));
console.timeEnd(`${N}!`);
