
let f = 1;
let i = 1;
while (i <= 20) {
  f *= i;
  i += 1;
}

console.time('20!');
console.log(f);
console.timeEnd('20!');
