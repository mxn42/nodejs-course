
var factorial = require('./factorial');

const N = process.argv[0];
console.time(`${N}!`);
console.log(factorial(N));
console.timeEnd(`${N}!`);
