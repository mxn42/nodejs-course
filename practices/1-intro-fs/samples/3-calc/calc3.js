
const fs = require('fs');
const path = require('path');
const operations = require('./operations');

const o = process.argv[2];
const inputFiles = process.argv.slice(3);
const outputFile = path.join(__dirname, 'output.txt');

if (!operations[o])
  throw TypeError(`Operation "${o}" is not supported`);

let fineRes;
let count = 0;

const checkComplete = (file, res) => {
  console.log(file, res);
  fineRes = (typeof fineRes === 'undefined')? res : operations[o](fineRes, res);
  count += 1;
  if (count === inputFiles.length)
    writeRes(fineRes);
};

console.time(`Operation ${o}`);
for (let file of inputFiles) {
  fs.readFile(path.join(__dirname, file), 'utf-8', (err, data) => {
    if (err) throw err;
    const numbers = data.split(/\r*\n/g).map(Number);
    const res = numbers.slice(1).reduce(operations[o], numbers[0]);
    checkComplete(file, res);
  });
}

function writeRes(data) {
  fs.writeFile(outputFile, data, 'utf-8', err => {
    if (err) throw err;
    console.timeEnd(`Operation ${o}`);
  });
}
