
const fs = require('fs');
const path = require('path');

console.time('sum');
fs.readFile(path.join(__dirname, 'input.txt'), 'utf-8', (err, data) => {
  if (err) throw err;
  const sum = data.split(/\r*\n/g).reduce((aggr, item) => aggr + Number(item), 0);
  fs.writeFile('output.txt', sum, 'utf-8', err => {
    if (err) throw err;
    console.timeEnd('sum');
  });
});
