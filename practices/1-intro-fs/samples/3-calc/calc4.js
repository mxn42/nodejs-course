
const fs = require('fs');
const path = require('path');
const operations = require('./operations');

const o = process.argv[2];
const inputFiles = process.argv.slice(3);
const outputFile = path.join(__dirname, 'output.txt');

if (!operations[o])
  throw TypeError(`Operation "${o}" is not supported`);

(async() => {
  let fineRes;
  console.time(`Operation ${o}`);
  for (let file of inputFiles) {
    const data = await readFile(file);
    const numbers = data.split(/\r*\n/g).map(Number);
    const res = numbers.slice(1).reduce(operations[o], numbers[0]);
    console.log(file, res);
    fineRes = (typeof fineRes === 'undefined')? res : operations[o](fineRes, res);
  }
  writeRes(fineRes);
})();


function readFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(__dirname, file), 'utf-8', (err, ...data) => {
      if (err) reject(err);
      else resolve(...data);
    });
  });
}

function writeRes(data) {
  fs.writeFile(outputFile, data, 'utf-8', err => {
    if (err) throw err;
    console.timeEnd(`Operation ${o}`);
  });
}
