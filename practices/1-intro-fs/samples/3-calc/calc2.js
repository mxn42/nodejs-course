
const fs = require('fs');
const path = require('path');
const operations = require('./operations');

const o = process.argv[2];
const inputFile = path.join(__dirname, process.argv[3]);
const outputFile = path.join(__dirname, process.argv[4]);

if (!operations[o])
  throw TypeError(`Operation "${o}" is not supported`);

console.time(`Operation ${o}`);
fs.readFile(inputFile, 'utf-8', (err, data) => {
  if (err) throw err;
  const numbers = data.split(/\r*\n/g).map(Number);
  const res = numbers.slice(1).reduce(operations[o], numbers[0]);
  fs.writeFile(outputFile, res, 'utf-8', err => {
    if (err) throw err;
    console.timeEnd(`Operation ${o}`);
  });
});
