
const factorial = N => {
  let f = 1;
  for (let i = 1; i <= N; i += 1) {
    f *= i;
  }
  return f;
};

module.exports = factorial;
