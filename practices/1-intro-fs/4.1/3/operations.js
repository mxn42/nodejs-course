
const operations = {
  '+': (a, b) => a + b,
  '-': (a, b) => a - b,
  '*': (a, b) => a * b,
  '/': (a, b) => a / b
};

function walk(o, numbers) {
  let res = numbers.shift();
  for (let item of numbers)
    res = operations[o](res, item)
  return res;
}

module.exports = {operations, walk};
