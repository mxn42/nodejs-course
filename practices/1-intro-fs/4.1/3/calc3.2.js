
// > node calc3.js input1.txt input2.txt

const fs = require('fs');

const finish = () => {
  console.timeEnd('Label1');
  console.log(sum);
};

console.time('Label1');
let sum = 0;
let countedFiles = 0;
const files = process.argv.slice(2);
for (let file of files) {
  fs.readFile(file, 'utf-8', (err, data) => {
    if (err) throw err;
    sum += Number(data);
    countedFiles += 1;
    if (countedFiles === files.length)
      finish();
  });
}

