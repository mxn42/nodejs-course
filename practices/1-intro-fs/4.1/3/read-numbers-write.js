
const fs = require('fs');

function readNumbersWrite(inputFile, fn, outputFile) {
  fs.readFile(inputFile, 'utf-8', (err, data) => {
    if (err) throw err;

    const numbers = data.trim().split(/\r*\n/).map(Number);
    const res = fn(numbers);

    fs.writeFile(outputFile, res, 'utf-8', err => {
      if (err) throw err;
    });
  });
}

module.exports = readNumbersWrite;
