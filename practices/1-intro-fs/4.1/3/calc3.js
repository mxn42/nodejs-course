
// > node calc3.js input1.txt input2.txt

const fs = require('fs');

console.time('Label1');
let sum = 0;
for (let file of process.argv.slice(2)) {
  let value = fs.readFileSync(file, 'utf-8');
  sum += Number(value);
}
console.timeEnd('Label1');
console.log(sum);
