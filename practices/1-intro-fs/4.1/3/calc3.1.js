
// > node calc3.js input1.txt input2.txt

const fs = require('fs');

const finish = () => {
  console.timeEnd('Label1');
  console.log(sum);
};

console.time('Label1');
let sum = 0;
fs.readFile(process.argv[2], 'utf-8', (err, data) => {
  if (err) throw err;
  sum += Number(data);
  fs.readFile(process.argv[3], 'utf-8', (err, data) => {
    if (err) throw err;
    sum += Number(data);
    fs.readFile(process.argv[4], 'utf-8', (err, data) => {
      if (err) throw err;
      sum += Number(data);
      finish();
    });
  });
});


