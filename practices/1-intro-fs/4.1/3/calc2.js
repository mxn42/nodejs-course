
const rnw = require('./read-numbers-write');
const {operations, walk} = require('./operations');

// > node calc2.js   + input.txt output.txt
const o = process.argv[2];  // + - * /
const inputFile = process.argv[3];
const outputFile = process.argv[4];

if (operations[o])
  rnw(inputFile, numbers => walk(o, numbers), outputFile);
else
  throw new TypeError(`Операция ${o} не поддерживается.`);
