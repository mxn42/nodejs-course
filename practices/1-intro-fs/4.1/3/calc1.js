
const fs = require('fs');

fs.readFile('input.txt', 'utf-8', (err, data) => {
  if (err) throw err;

  const numbers = data.trim().split(/\r*\n/).map(Number);

  let sum = 0;
  for (let item of numbers)
    sum += item;

  fs.writeFile('output.txt', sum, 'utf-8', err => {
    if (err) throw err;
  });
});
