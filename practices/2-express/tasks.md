### Практика 5

# Express

Документация: <http://expressjs.com/ru/>

## 1. Hello, timeserver, echo
   
На одном сервере реализовать веб-сервисы "Hello, world", timeserver, эхо-сервер на express.js по адресам: `GET /` возвращает "Hello, World!", `GET /time` возвращает дату и время сервера, `GET /echo?say=YAHOOOOO` возвращает значение `say` в запросе.
   
## 2. Статичные файлы
   
Реализовать веб-сервер, возвращающий статичный сайт в папке `www`. Файлы могут быть любых типов, включая html, css, jpeg, js, json, mp4.

## 3.1. ToDo list: RESTful CRUD.
   <http://expressjs.com/ru/starter/basic-routing.html>
   
   http://expressjs.com/ru/guide/routing.html#response-methods
   
   http://expressjs.com/ru/4x/api.html#req.body
  
## 3.2. Интерфейс запросов к API 

  
## 4. Разделение роутинга и бизнес-логики

