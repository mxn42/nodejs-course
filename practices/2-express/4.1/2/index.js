
const bodyParser = require('body-parser');
const express = require('express');
const app = express();

app.use(express.static('www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const todos = [
  {id: 111, text: 'Сделать раз'},
  {id: 121, text: 'Сделать два'},
  {id: 144, text: 'Сделать три'},
];
let nextId = 150;

// CREATE

app.post('/api/todos', (req, res) => {
  const text = req.body.text;
  const newElement = {
    id: nextId++,
    text: text
  };
  todos.push(newElement);
  res.json(newElement);
});

// READ

app.get('/api/todos', (req, res) => {
  res.json(todos);
});

app.get('/api/todos/:id', (req, res) => {
  const el = todos.find(el => el.id === Number(req.params.id));
  if (el)
    res.json(el);
  else
    res.status(404).send(`Element "${req.params.id}" not found.`);
});

// UPDATE

app.put('/api/todos/:id', (req, res) => {
  const el = todos.find(el => el.id === Number(req.params.id));
  if (el) {
    const newElement = req.body.newElement;
    todos.splice(todos.indexOf(el), 1, newElement);
    res.status(204).send(`Element has been updated.`);
  }
  else
    res.status(404).send(`Element "${req.params.id}" not found.`);
});

app.patch('/api/todos/:id', (req, res) => {
  const el = todos.find(el => el.id === Number(req.params.id));
  if (el) {
    el.text = req.body.text;
    res.status(204).send(`Element has been updated.`);
  }
  else
    res.status(404).send(`Element "${req.params.id}" not found.`);
});

// DELETE

app.delete('/api/todos/:id', (req, res) => {
  const el = todos.find(el => el.id === Number(req.params.id));
  if (el) {
    todos.splice(todos.indexOf(el), 1);
    res.status(204).send(`Element has been deleted.`);
  }
  else
    res.status(404).send(`Element "${req.params.id}" not found.`);
});

app.listen(5533, () => {
  console.log("Listen on :5533");
});
