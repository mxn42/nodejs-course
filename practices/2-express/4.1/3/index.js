
const bodyParser = require('body-parser');
const express = require('express');
const todos = require('./todos');

const app = express();

app.use(express.static('www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/todos', todos.create);
app.get('/api/todos', todos.list);
app.get('/api/todos/:id', todos.read);
app.put('/api/todos/:id', todos.put);
app.patch('/api/todos/:id', todos.update);
app.delete('/api/todos/:id', todos.delete);

app.listen(5533, () => {
  console.log("Listen on :5533");
});
