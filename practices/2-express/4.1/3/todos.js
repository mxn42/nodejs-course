const todos = require('./todos.json');
const fs = require(fs);

const save = () => {
  fs.writeFile('./todos.json', JSON.stringify(todos, null, 2), 'utf-8', (err) => {
    if (err) console.error(err);
  });
};

module.exports = {
  create: (req, res) => {
    const text = req.body.text;
    const newElement = {
      id: todos.nextId++,
      text: text
    };
    todos.data.push(newElement);
    save();
    res.json(newElement);
  },

  list: (req, res) => {
    res.json(todos.data);
  },

  read: (req, res) => {
    const el = todos.data.find(el => el.id === Number(req.params.id));
    if (el)
      res.json(el);
    else
      res.status(404).send(`Element "${req.params.id}" not found.`);
  },

  put: (req, res) => {
    const el = todos.data.find(el => el.id === Number(req.params.id));
    if (el) {
      const newElement = req.body.newElement;
      todos.data.splice(todos.data.indexOf(el), 1, newElement);
      save();
      res.status(204).send(`Element has been updated.`);
    }
    else
      res.status(404).send(`Element "${req.params.id}" not found.`);
  },

  update: (req, res) => {
    const el = todos.data.find(el => el.id === Number(req.params.id));
    if (el) {
      el.text = req.body.text;
      save();
      res.status(204).send(`Element has been updated.`);
    }
    else
      res.status(404).send(`Element "${req.params.id}" not found.`);
  },

  delete: (req, res) => {
    const el = todos.data.find(el => el.id === Number(req.params.id));
    if (el) {
      todos.data.splice(todos.data.indexOf(el), 1);
      save();
      res.status(204).send(`Element has been deleted.`);
    }
    else
      res.status(404).send(`Element "${req.params.id}" not found.`);
  }
};
