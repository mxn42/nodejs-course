
const express = require('express');

const app = express();
app.use(express.static('public'));

app.get('/', function(request, response) {
  response.send('Hello, world!');
});

app.get('/dice', function(request, response) {
  const sides = Number(request.query.sides) || 6;
  const count = Number(request.query.count) || 1;
  const dices = [];
  for (let i = 0; i < count; i += 1)
    dices.push(Math.floor(Math.random() * sides) + 1);
  response.send(dices.join(' '));
});

//   /calc?a=42&b=24&o=
app.get('/calc', function(request, response) {
  const a = request.query.a;
  const b = request.query.b;
  const c = (o === '*')? a * b : a + b;
  response.send(c);
});

app.get('/time', (req, res) => {
  res.send(new Date().toLocaleTimeString());
});

app.get('/echo', (req, res) => {
  const url = req.originalUrl;
  res.send(url);
});



app.listen(7755, () => {
  console.log('Сервер запущен, очень запущен.');
});

