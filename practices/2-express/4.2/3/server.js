
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(express.static('www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const todos = [
  {"id": 12, "text": 'cделать раз'},
  {"id": 23, "text": 'сделать два'},
  {"id": 135, "text": 'сделать три'}
];
let nextId = 200;

//  C  - create
app.post('/todos', (req, res) => {
  const text = req.body;
  const element = {
    id: nextId++,
    text: text
  };
  todos.push(element);
  res.json(element);
});

//  R  - read
app.get('/todos', (req, res) => {
  res.json(todos);
});

app.get('/todos/:id', (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    res.json(el.text);
  }
  else {
    res.status(404).send('Element not found');
  }
});

//  U  - update
app.patch('/todos/:id', (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    const text = req.body;
    el.text = text;
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
});
app.put('/todos/:id', (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    const newElement = req.body;
    todos.splice(todos.indexOf(el), 1, newElement);
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
});

//  D  - delete
app.delete('/todos/:id', (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    todos.splice(todos.indexOf(el), 1);
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
});


app.listen(8080);

