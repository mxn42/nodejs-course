
const todos = [
  {"id": 12, "text": 'cделать раз'},
  {"id": 23, "text": 'сделать два'},
  {"id": 135, "text": 'сделать три'}
];

let nextId = 200;

const create = (req, res) => {
  const text = req.body;
  const element = {
    id: nextId++,
    text: text
  };
  todos.push(element);
  res.json(element);
};

const list = (req, res) => {
  res.json(todos);
};

const read = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    res.json(el.text);
  }
  else {
    res.status(404).send('Element not found');
  }
};

const patch = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    const text = req.body;
    el.text = text;
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
};

const put = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    const newElement = req.body;
    todos.splice(todos.indexOf(el), 1, newElement);
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
};

const remove = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.find(el => el.id === id);
  if (el) {
    todos.splice(todos.indexOf(el), 1);
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
}

module.exports = {
  create: create,
  list: list,
  read: read,
  patch: patch,
  put: put,
  remove: remove
};
