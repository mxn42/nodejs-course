
const express = require('express');
const app = express();

app.use(express.static('www'));

app.get('/', (request, response) => {
  response.send('Hello, World!');
});

//   http://localhost:54321/dice?sides=20&count=100
app.get('/dice', (request, response) => {
  const sides = Number(request.query.sides) || 6;
  const count = Number(request.query.count) || 1;
  const rolls = [];
  for (let i = 0; i < count; i += 1) {
    rolls.push(Math.floor(Math.random() * sides) + 1);
  }
  response.send(rolls.join(' '));
});

app.listen(54321, () => {
  console.log('Сервер слушает порт :54321');
});
