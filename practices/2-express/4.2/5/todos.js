
const todos = require('./data');
const fs = require('fs');

const save = () => {
  fs.write('./data.json', JSON.stringify(todos, null, 2), 'utf-8', (err) => {
    if (err) throw err;
  });
};

const create = (req, res) => {
  const message = req.body.message;
  const author = req.body.author;
  const data = new Date;
  const element = {
    id: todos.nextId++,
    message,
    author,
    data
  };
  todos.data.push(element);
  save();
  res.json(element);
};

const list = (req, res) => {
  res.json(todos.data);
};

const read = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.data.find(el => el.id === id);
  if (el) {
    res.json(el.text);
  }
  else {
    res.status(404).send('Element not found');
  }
};

const patch = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.data.find(el => el.id === id);
  if (el) {
    const text = req.body;
    el.text = text;
    save();
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
};

const put = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.data.find(el => el.id === id);
  if (el) {
    const newElement = req.body;
    todos.data.splice(todos.data.indexOf(el), 1, newElement);
    save();
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
};

const remove = (req, res) => {
  const id = Number(req.params.id);
  const el = todos.data.find(el => el.id === id);
  if (el) {
    todos.data.splice(todos.data.indexOf(el), 1);
    save();
    res.status(204).send();
  }
  else {
    res.status(404).send('Element not found');
  }
};

module.exports = {
  create: create,
  list: list,
  read: read,
  patch: patch,
  put: put,
  remove: remove
};
