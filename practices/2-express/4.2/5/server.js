
const express = require('express');
const bodyParser = require('body-parser');
const todos = require('./todos');

const app = express();
app.use(express.static('www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/todos', todos.create);
app.get('/todos', todos.list);
app.get('/todos/:id', todos.read);
app.patch('/todos/:id', todos.patch);
app.put('/todos/:id', todos.put);
app.delete('/todos/:id', todos.remove);

app.listen(8080);

