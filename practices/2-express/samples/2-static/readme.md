
## Статичные файлы

Реализовать веб-сервер, возвращающий статичный сайт в папке `www`. Файлы могут быть любых типов, включая html, css, jpeg, js, json, mp4.

### Подсказки

Документация и примеры: <http://expressjs.com/ru/starter/static-files.html>.

  ```javascript
    app.use(express.static('www'));
  ```

Несколько папок

  ```javascript
    app.use(express.static('public'));
    app.use(express.static('files'));
  ```

Можно указать адрес (url), по которому нужно возвращать файлы из папки.
Например, после определения ниже запрос `GET /photos/1.jpg` вернет файл `media/foto/public/1.jpg`.

  ```javascript
    app.use('/photos', express.static('media/foto/public'));
  ```

На одном сервере можно расположить разные сайты, указав для них пути

  ```javascript
    app.use('/cats', express.static('hobby/sites/cats/htdata'));
    app.use('/portfolio', express.static('work/portfolio/wwww'));
  ```

**Замечание**. Следует быть внимательным в указании пути к папкам. Он определяется относительно пути, где скрипт запущен (а не где он располагается). Путь можно указать абсолютным, например `express.static('/usr/vasya/wwww')`, или относительно расположения скрипта `express.static(__dirname + '/public'))`. 
