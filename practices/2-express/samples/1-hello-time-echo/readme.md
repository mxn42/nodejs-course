
## Hello, timeserver, echo

На одном сервере реализовать веб-сервисы "Hello, world", timeserver, эхо-сервер на express.js по адресам: `GET /` возвращает "Hello, World!", `GET /time` возвращает дату и время сервера, `GET /echo?say=YAHOOOOO!` возвращает значение `say` в запросе.


### Подсказки

* Установка Express: <http://expressjs.com/ru/starter/installing.html>

  > npm install express --save

* "Hello, world": <http://expressjs.com/ru/starter/hello-world.html>

  ```javascript
    var express = require('express');
    var app = express();
    
    app.get('/', function (req, res) {
      res.send('Hello World!');
    });
    
    app.listen(3000, function () {
      console.log('Example app listening on port 3000!');
    });
  ```

* Вывод времени

  ```javascript
    var now = new Date();
    res.send(now.toLocaleString());
  ```
    
* Разбор запроса `GET /people?name=Milton&surname=Friedman` в Express

  ```javascript
    app.get('/people', function (req, res) {
      var firstname = req.query.name;
      var lastname = req.query.surname;
    });
  ```
