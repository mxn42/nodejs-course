
## CRUD API для ToDo

Написать API для списка дел. Необходимо поддерживать: создание элемента (Create), получение элемента (Read), исправление элемента (Update) и удаление (DELETE), кроме этого необходим возвращать список всех этих элементов.


### Подсказки

#### REST

Необходимо реализовать REST методы API:

| Запрос            | Результат          | Код |
|---|---|---|
| `GET /todo`       | Список всех        | 200 |
| `POST /todo`      | Создание элемента  | 201 |
| `GET /todo/:id`   | Список всех        | 200 |
| `PUT /todo/:id`   | Изменение элемента | 200 |
| `DELETE /todo:id` | Удаление элемента  | 200 |

За основу возьмем пример из документации <http://expressjs.com/ru/starter/basic-routing.html>

```javascript
  app.get('/todo', function (req, res) {
    // вернуть список
  });
```

```javascript
  app.post('/todo', function (req, res) {
    // создать элемент
  });
```

```javascript
  app.get('/todo/:id', function (req, res) {
    var id = req.params.id;
    // вернуть элемент по id
  });
```

```javascript
  app.put('/todo/:id', function (req, res) {
    var id = req.params.id;
    // обновить элемент по id
  });
```

```javascript
  app.delete('/todo/:id', function (req, res) {
    var id = req.params.id;
    // удалить элемент по id
  });
```

#### Методы возврата

Пусть список представляет собой массив.

```javascript
var todo = [
  { "id": 101, "checked": true, "text": "Wake up" },
  { "id": 102, "checked": true, "text": "Have a breakfast" },
  { "id": 103, "checked": true, "text": "Work" },
  { "id": 104, "checked": true, "text": "Have a lunch" }
]
```
   
Для возврата значения клиенту в Express существует несколько методов: <http://expressjs.com/ru/guide/routing.html#response-methods>, для наших целей наиболее предпочтительными являются `res.send()` и `res.json`.

Например, первый ответ (список всех) может выглядеть так

```javascript
  app.get('/todo', function (req, res) {
    res.json(todo);
  });
```

#### Обработка POST
   
Для получения свойств, оправленных с помощью методов POST и PUT необходим установить дополнительный модуль `body-parser`. Пример использования -- в документации свойство body объекта Request: <http://expressjs.com/ru/4x/api.html#req.body>
   
```javascript
  var app = require('express')();
  var bodyParser = require('body-parser');
  
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true })); 

  // создание    
  app.post('/todo', function (req, res) {
    var checked = (req.body.checked == 'on');
    var text =     req.body.text;
    // создание элемента
  });
  
  // создание    
  app.put('/todo/:id', function (req, res) {
    var id = req.params.id;
    var checked = (req.body.checked == 'on');
    var text =     req.body.text;
    // обновление элемента
  });
```

#### Передача управления
