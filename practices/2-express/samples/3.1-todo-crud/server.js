
var express = require('express');
var app = express();

app.listen(5300, function () {
  console.log('Server is running on http://localhost:5300/');
});

app.use(express.static(__dirname + '/www'));
