
var http = require('http');

// Создать http-сервер
var server = http.createServer();

// Слушать
server.listen(10001);
console.log("I'm listening 10001.");

// Отвечать на запрос
server.on('request', handler);

// Обработать запрос
function handler(request, response) {
	console.log("------------------");
	console.log(request.method, request.url);
	
	response.end("Hello, World!");
}










