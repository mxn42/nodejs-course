
var http = require('http');
var random = require('./random');

var server = http.createServer();

server.listen(10002);
console.log('Server is listening 10002');


server.on('request', function(request, response) {
	console.log('-------------------------------------');
	console.log(request.method, request.url);
	if (request.url.substring(0, 6) == '/dice/') {
		var N = Number(request.url.substring(6));
		var x = random.dice(N);
		response.end(String(x));
		console.log('Return:', String(x));
	}
	else {
		response.statusCode = 404;
		response.end('Not found');
		console.log('Return:', 'Not found');
	}
});
