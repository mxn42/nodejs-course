var http = require('http');
var url = require('url');
var random = require('./random');

var server = http.createServer();
server.listen(10005);
console.log('Server is listening 10005');

server.on('request', function(request, response) {
	console.log('-------------------------------------');
	console.log(request.method, request.url);
	
	var parsed = url.parse(request.url, true);
	
	if (parsed.pathname == '/dice') {
		var N = parsed.query.sides;
		var count = parsed.query.count;
		var x = random.rolls(N, count);
		response.end(String(x));
		console.log('Return:', String(x));
	}
	else {
		response.statusCode = 404;
		response.end('Not found');
		console.log('Return:', 'Not found');
	}
});
