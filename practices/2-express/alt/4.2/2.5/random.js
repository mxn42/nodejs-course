
function MEGAdice(N) {
	return Math.floor(Math.random() * N) + 1;
}

function MEGArolls(N, count) {
	var dices = [];
	for (var i = 0; i < count; i += 1) {
		dices.push(MEGAdice(N));
	}
	return dices.join(' ');
}

// module
// module.exports
//        exports

exports.dice = MEGAdice; 
exports.rolls = MEGArolls; 
