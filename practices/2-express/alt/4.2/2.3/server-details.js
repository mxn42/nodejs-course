// подключение модулей
var http = require('http');

// создаем сервер
var server = http.createServer();

// сервер слушает порт
server.listen(10002);
console.log('Server is listening 10002');

// подписка на событие
server.on('request', function(request, response) {
	// логгирование
	console.log('-------------------------------------');
	console.log(request.method, request.url);
	// роутинг
	if (request.url.substring(0, 6) == '/dice/') {
		// получаем параметры
		var N = Number(request.url.substring(6));
		// находим решение
		var x = dice(N);
		// возвращаем ответ клиенту
		response.end(String(x));
		// логгирование
		console.log('Return:', String(x));
	}
	else {
		// реакция на неверный путь
		response.statusCode = 404;
		response.end('Not found');
		console.log('Return:', 'Not found');
	}
});


function dice(N) {
	return Math.floor(Math.random() * N) + 1;
}

