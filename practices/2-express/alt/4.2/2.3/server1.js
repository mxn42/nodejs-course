
var http = require('http');

var server = http.createServer();

server.listen(10002);
console.log('Server is listening 10002');

function dice(N) {
	return Math.floor(Math.random() * N) + 1;
}

function responseDice(N, response) {
	var x = dice(N);
	response.end(String(x));
	console.log('Return:', String(x));
}

server.on('request', function(request, response) {
	console.log('-------------------------------------');
	console.log(request.method, request.url);
	
	if (request.url == '/dice/6') {
		responseDice(6, response)
	}
	if (request.url == '/dice/8') {
		responseDice(8, response)
	}
	if (request.url == '/dice/32') {
		responseDice(32, response)
	}
	else {
		response.statusCode = 404;
		response.end('Not found');
		console.log('Return:', 'Not found');
	}
	
});
