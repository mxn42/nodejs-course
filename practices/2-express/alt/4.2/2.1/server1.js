
var http = require('http');

var server = http.createServer();

server.listen(10002);
console.log('Server is listening 10002');

server.on('request', function(request, response) {
	console.log('-------------------------------------');
	console.log(request.method,  request.url);
	
	var x = Math.floor(Math.random() * 6) + 1;
	response.end(String(x));
	console.log('Return:', String(x));
});
