
var http = require('http');

http.createServer(function(request, response) {
  console.log(request.method, request.url);
  if (request.url == '/dice') {
    var value = Math.floor(Math.random() * 6) + 1;
    response.end('' + value);
  }
  else {
    response.statusCode = 404;
    response.end('Not found');
  }

}).listen(8080);
