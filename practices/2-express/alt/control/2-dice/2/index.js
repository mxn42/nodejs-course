
var http = require('http');
var url = require('url');

http.createServer(function(request, response) {
  console.log(request.method, request.url);
  var reqUrl = url.parse(request.url, true);
  if (reqUrl.pathname == '/dice') {
    var sides = Number(reqUrl.query.sides) || 6;
    var value = Math.floor(Math.random() * sides) + 1;
    console.log("Response: %s from %s", value, sides);
    response.end(String(value));
  }
  else {
    response.statusCode = 404;
    response.end('Not found');
  }

}).listen(8080);
