
var http = require('http');
var url = require('url');

http.createServer(function(request, response) {
  console.log(request.method, request.url);
  var reqUrl = url.parse(request.url, true);
  if (reqUrl.pathname == '/dice') {
    var sides = Number(reqUrl.query.sides) || 6;
    var count = Number(reqUrl.query.count) || 1;
    var values = [];
    for (var i = 0; i < count; i += 1) {
      values[i] = Math.floor(Math.random() * sides) + 1;
    }
    console.log("Response: [%s] from %s", values, sides);
    response.end(values.join(' '));
  }
  else {
    response.statusCode = 404;
    response.end('Not found');
  }

}).listen(8080);
