
var http = require('http');
var server = new http.Server();

server.listen(8081, '127.0.0.1');

server.on('request', function(request, response) {
  console.log(request.method, request.url);
  response.end('B) Hello, World!');
});

var emitOriginal = server.emit;
server.emit = function(event) {
  console.log('Event:', event);
  emitOriginal.apply(server, arguments);
};

console.log('HTTP server running.');
