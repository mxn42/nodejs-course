
var http = require('http');

function listenHandler(request, response) {
  console.log(request.method, request.url);
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end('C) Hello, World!');
}

var server = http.createServer(listenHandler);

server.listen(8081, '127.0.0.1');

console.log('HTTP server running.');
