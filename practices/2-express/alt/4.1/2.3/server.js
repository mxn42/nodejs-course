
var http = require('http');
var url  = require('url');
var dice = require('./dice');

http.createServer(function(request, response) {
	var parsed = url.parse(request.url, true);
	console.log('------ Request -------');
	console.log(request.method, parsed.pathname, parsed.query);
	
	if (parsed.pathname == '/dice') {
		var N = parsed.query.sides;
		var diceN = dice(N);
		var x = diceN.roll();
		response.end(String(x));
	}
	else {
		response.statusCode = 404;
		response.end("Not Found");
	}

}).listen(7772);
console.log('Server is running');
