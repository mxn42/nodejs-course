
var http = require('http');

var server = new http.Server();

server.on('request', function(request, response) {
	response.end('Hello! Maxim here.');
});

server.listen(7777);
console.log('Server is running');