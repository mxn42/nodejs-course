
var http = require('http');

var server = http.createServer();

server.listen(7777);
console.log('Server is running');

server.on('request', requestHandler);

function requestHandler(request, response) {
	response.end('Hello! Maxim here.');
}

