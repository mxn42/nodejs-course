
var http = require('http');

http.createServer(function(request, response) {
	console.log(request.url);
	if (request.url == '/favicon.ico') {
		response.end();
		return;
	}
	var url = request.url.substr(1);
	var N = Number(url);
	var x = Math.floor(Math.random() * N) + 1;
	response.end(String(x));
}).listen(7772);

console.log('Server is running');
