
// [0..1)
x = Math.random();

// [0..6)
x = x * 6;

// [0, 1, 2, 3, 4, 5]
x = Math.floor(x);

// [1, 2, 3, 4, 5, 6]
x = x + 1;

console.log(x);
