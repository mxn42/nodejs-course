

var random = require('./random');

var http = require('http');
var url = require('url');
var random = require('./random');

var server = http.createServer();

server.listen(7001);
console.log('Server is running on 7001');

server.on('request', function(request, response) {
	console.log('-------------------------------------');
	console.log(request.method, request.url);
	
	var parsed = url.parse(request.url, true);
	
	if (parsed.pathname == '/dice') {
		var N     = Number(parsed.query.sides);
		var count = Number(parsed.query.count);
		var x = random.rolls(N, count);
		response.end(String(x));
		console.log('Return:', x);
	}
	else {
		response.statusCode = 404;
		response.end('Not found');
		console.log('Return: Not found');
	}
});

