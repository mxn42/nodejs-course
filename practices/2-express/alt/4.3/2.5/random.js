
function dice(N) {
	return Math.floor(Math.random() * N) + 1;
}

function rolls(N, count) {
	var dices = [];
	for (var i = 0; i < count; i++) {
		dices.push(dice(N));
	}
	return dices.join(' ');
}

exports.dice = dice;
exports.rolls = rolls;
