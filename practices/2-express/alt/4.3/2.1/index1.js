
// R [0..1)
var x = Math.random();

// R [0..6)
x = x * 6;

// N [0..6), то есть [0, 1, 2, 3, 4, 5]
x = Math.floor(x);

// [1, 2, 3, 4, 5, 6]
x = x + 1;

console.log(x);
