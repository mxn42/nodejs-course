
var http = require('http');

// создать сервер
var server = http.createServer();

// сервер слушает порт
server.listen(7701);

// сервер отвечает на запрос
server.on('request', function(request, response) {
	response.end('Hello, World!');
});
