
# Модуль dice

# Использование

```
  var dice = require('./dice');
  
  var dice6 = dice.generate(6); // шестигранный кубик
  console.log(dice6.roll());    // бросок кубика
  
  console.log(dice.rolls(8, 3)); // 3 броска восьмигранного кубика
```
	
	
	