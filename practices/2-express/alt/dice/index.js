
function random(N) {
	return Math.floor(Math.random() * N) + 1;
}

function generate(N) {
	return {
		roll: function() { 
			return random(N); 
		}
	}
}

function rolls(N, count) {
	var diceN = generate(N);
	var values = [];
	for (var i = 0; i < count; i++) {
		values[i] = diceN.roll();
	}

	return values.join(' ');
}

exports.generate = generate;
exports.rolls = rolls;
