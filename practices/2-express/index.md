
# Express

  http://expressjs.com/ru/
  
  app.METHOD(PATH, HANDLER)

    app - это экземпляр express.
    METHOD - метод запроса HTTP.
    PATH - путь на сервере.
    HANDLER - функция, выполняемая при сопоставлении маршрута.
    
  Маршрутизация
  http://expressjs.com/ru/guide/routing.html  
  
  next() зачем это нужно
    - кэширование
    - CORS
    - авторизация
    
  пакеты обработчиков
  app.get('/example/c', [cb0, cb1, cb2]);
  
  
  
## Методы ответа
  <http://expressjs.com/ru/guide/routing.html>

  Методы в объекте ответа (res), перечисленные в таблице ниже, могут передавать ответ клиенту и завершать цикл “запрос-ответ”. Если ни один из этих методов не будет вызван из обработчика маршрута, клиентский запрос зависнет.
  
  |Метод	|Описание|
  |---|---|
  |res.download()	|Приглашение загрузки файла.
  |res.end()	|Завершение процесса ответа.
  |res.json()	|Отправка ответа JSON.
  |res.jsonp()	|Отправка ответа JSON с поддержкой JSONP.
  |res.redirect()	|Перенаправление ответа.
  |res.render()	|Вывод шаблона представления.
  |res.send()	|Отправка ответа различных типов.
  |res.sendFile	|Отправка файла в виде потока октетов.
  |res.sendStatus()	|Установка кода состояния ответа и отправка представления в виде строки в качестве тела ответа.
    
## app.route()  
  app.route('/book')
    .get(function(req, res) {
      res.send('Get a random book');
    })
    .post(function(req, res) {
      res.send('Add a book');
    })
    .put(function(req, res) {
      res.send('Update the book');
    });
    
    
## express.Router    
  var router = express.Router();  
    
  
  