
var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.listen(5300, function () {
  console.log('Example app listening on port http://localhost:5300/');
});

var tasks = require('./tasks.json');
var todo = tasks.tasks;
var nextId = tasks.nextId;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('www'));
// http://localhost:5300/create.html

app.get('/todo', function (req, res) {
  res.json(todo);
});

app.post('/todo', function (req, res) {
  console.log(req.body);
  var task = {
    "id":       nextId++,
    "checked": (req.body.checked == 'on'),
    "text":     req.body.text
  };
  todo.push(task);
  res.statusCode = 201;
  res.json(task);
  update();
});


app.all('/todo/:id', function(req, res, next) {
  var id = req.params.id;
  var task = todo.find(function(task) { return task.id == id });
  if (task) {
    req.task = task;
    next();
  }
  else {
    res.statusCode = 404;
    res.send('Not found');
  }
});

app.get('/todo/:id', function (req, res) {
  res.json(req.task);
});

app.put('/todo/:id', function (req, res) {
  req.task.checked = (req.body.checked == 'on');
  req.task.text    = req.body.text;
  res.json(req.task);
  update();
});

app.delete('/todo/:id', function (req, res) {
  tasks.remove(req.task);
  res.end();
  update();
});

function update() {
  var text = JSON.stringify({
    nextId: nextId,
    tasks: todo
  });
  fs.writeFile('tasks.json', text, function(err) {
    if (err) {
      console.error('Не записалось', err);
    }
  });
}
