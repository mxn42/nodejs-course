
var express = require('express');
var app = express();

app.use(express.static('public'));

app.listen(5300, function () {
  console.log('Example app listening on port http://localhost:5300/');
});

