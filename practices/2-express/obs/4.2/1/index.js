
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/time', function(req, res) {
  res.send(new Date().toLocaleString());
});

app.get('/echo', function(req, res) {
  res.send(req.query.say);
});

app.listen(5300, function () {
  console.log('Example app listening on port http://localhost:5300/');
});

