
var fs = require('fs');

var data = require('./tasks.json');
var todo = data.tasks;
var nextId= data.nextId;

function getList(req, res) {
  res.json(todo);
}

function create(req, res) {
  var task = {
    id:      nextId++,
    checked: (req.body.checked == 'on'),
    text:     req.body.text
  };
  todo.push(task);
  res.json(task);
  update();
}



app.all('/todo/:id', function (req, res, next) {
  var id = req.params.id;
  var task = todo.find(function(task) { return task.id == id });
  if (task) {
    req.foundTask = task;
    next();
  }
  else {
    res.statusCode = 404;
    res.send('Not found');
  }
});

app.get('/todo/:id', function (req, res) {
  res.json(req.foundTask);
});

app.put('/todo/:id', function (req, res) {
  req.foundTask.checked = (req.body.checked == 'on');
  req.foundTask.text    =  req.body.text;
  res.json(req.foundTask);
  update();
});

app.delete('/todo/:id', function (req, res) {
  var index = todo.indexOf(req.foundTask);
  todo.splice(index, 1);
  res.send('Deleted');
  update();
});

function update() {
  fs.writeFile('./tasks.json', JSON.stringfy({
    nextId: nextId,
    tasks: todo
  }, 4));
}

exports.getList = getList;
exports.create = create;