
var express = require('express');
var bodyParser = require('body-parser');
var app = express().listen(5300);

var todo = require('./todo');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('www'));

app.get('/todo', todo.getList);
app.post('/todo', todo.create);
app.all('/todo/:id', todo.verify);
app.get('/todo/:id', todo.read);
app.put('/todo/:id', todo.update);
app.delete('/todo/:id', todo.delete);

