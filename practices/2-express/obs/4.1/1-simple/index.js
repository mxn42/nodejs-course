
var express = require('express');
var app = express();

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/time', function (req, res) {
  var date = new Date();
  res.send(date.toLocaleString());
});

app.get('/echo', function (req, res) {
  res.send(req.url);
});

app.listen(5500, function () {
  console.log('Express is running on 5500');
});
