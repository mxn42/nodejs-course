
var express = require('express');
var app = express();

app.use('/f', express.static(__dirname + '/public'));

app.listen(5550, function () {
  console.log('Express is running on http://localhost:5550');
});
