
var express = require('express');
var app = express();

var tasks = require('./tasks.json');

app.get('/todo', function (req, res) {
  res.json(tasks);
});

app.post('/todo', function (req, res) {
  res.send('New item');
});

app.put('/todo', function (req, res) {
  res.send('Update item');
});

app.delete('/user', function (req, res) {
  res.send('Delete item');
});

app.listen(5550, function () {
  console.log('Express is running on 5550');
});
