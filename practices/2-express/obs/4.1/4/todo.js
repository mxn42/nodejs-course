
var tasks = require('./tasks.json');

function list(req, res) {
  res.json(tasks);
}

function add(req, res) {
  console.log('add', req.body, req.query, req.params);
  tasks.push({
    checked: false,
    text: "TEST"
  });
  res.statusCode = 201;
  res.end('Created');
}

function get(req, res) {
  res.json(req.task);
}

function modify(req, res) {
  tasks[req.id].checked = req.query.checked;
  tasks[req.id].text    = req.query.text;
  res.end('Updated');
}

function remove(req, res) {
  tasks.splice(req.id, 1);
  res.end('Deleted');
}

function exists(req, res, next) {
  var id = req.params.id;
  var task = tasks[id];
  if (!task) {
    res.status = 404;
    res.end('Not found');
  }
  else {
    req.id = id;
    req.task = task;
    next();
  }
}

exports.create = add;
exports.read = get;
exports.update = modify;
exports.delete = remove;
exports.list = list;
exports.exists = exists;
