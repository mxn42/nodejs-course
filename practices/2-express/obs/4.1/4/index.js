
var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var todo = require('./todo');

app.get('/', function(req, res) {
  res.end('ToDo list');
});

app.use('/forms', express.static('./forms'));
app.use(bodyParser.json())

app.get('/todo',        todo.list);
app.post('/todo',       todo.create);

app.all('/todo/:id',    todo.exists);
app.get('/todo/:id',    todo.read);
app.put('/todo/:id',    todo.update);
app.delete('/todo/:id', todo.delete);

app.listen(5300, function () {
  console.log('Express is running on http://localhost:5300');
});
