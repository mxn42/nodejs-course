const express = require('express');
const route = require('./route');
const app = express();

app.use(express.static('www'));
app.get('/shorten', route.shorten);
app.get('/:short', route.goto);

app.listen(5555);
