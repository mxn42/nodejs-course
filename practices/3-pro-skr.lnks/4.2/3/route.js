
const model = require('./model');

function shorten(req, res) {
  const long = req.query.url;
  const link = model.linkByURL(long);
  res.json(link);
}

function goto(req, res) {
  const short = req.params.short;
  let link = model.linkByShort(short);
  if (link) {
    res.redirect(302, link.url);
  }
  else {
    res.status(404).send('URL not found.');
  }
}

module.exports = {
  shorten,
  goto
};
