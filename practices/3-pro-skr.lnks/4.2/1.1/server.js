const express = require('express');
const app = express();

app.use(express.static('www'));

const fs = require('fs');
const links = require('./links.json');

function save() {
  fs.writeFile(
      __dirname + '/links.json',
      JSON.stringify(links, null, 2),
      'utf-8',
      err => { if (err) console.error(err) }
  )
}

const A = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const generate = (len = 5) => {
  while (true) {
    let short = Array(len).fill(1).reduce(str => str + A[Math.floor(Math.random() * A.length)], '');
    if (!links.data.find(x => x.short === short))
      return short;
  }
};

app.get('/shorten', (req, res) => {
  const long = req.query.url;
  let link = links.data.find(x => x.url === long);
  if (!link) {
    link = {
      short: generate(),
      url: long,
      created: new Date().getTime(),
      redirects: 0
    };
    links.data.push(link);
    save();
  }
  res.json(link);
});

app.get('/:short', (req, res) => {
  const short = req.params.short;
  let link = links.data.find(x => x.short === short);
  if (link) {
    res.redirect(302, link.url);
    link.redirects += 1;
    save();
  }
  else {
    res.status(404).send('URL not found.');
  }
});
