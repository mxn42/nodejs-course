
const express = require('express');
const path = require('path');
const fs = require('fs');

const app = express();

const links = require('./links.json');

function save() {
  fs.writeFile(
      path.join(__dirname, './links.json'),
      JSON.stringify(links, null, 2),
      'utf-8',
      function(err) {
        if (err) throw err;
      }
  );
}

const symbols = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const generate = (len = 5) => {
  return Array(len)
      .fill(1)
      .reduce(str => str + symbols[Math.floor(Math.random() * symbols.length)], '');
};
// https://codeshare.io/G8nNN4
app.get('/shorten', (req, res) => {
  const url = decodeURIComponent(req.query.url);
  let link = links.data.find(x => x.url === url);
  if (!link) {
    let short = generate();
    while (links.data.find(x => x.short === short)) {
      short = generate();
    }
    link = {
      short: short,
      url: url,
      create: new Date().getTime(),
      redirects: 0
    };
    links.data.push(link);
    save();
  }
  res.json(link);
});
app.get('/:short', (req, res) => {
  const short = req.params.short;
  const link = links.data.find(x => x.short === short);
  if (link) {
    // res.send(link.url);
    //        301    link.url
  }
  else {
    res.status(404).send();
  }
});

app.listen(8080, () => {
  console.log('http://localhost:8080/')
});
