
const links = require('./links.json');

function save() {
  fs.writeFile(
      path.join(__dirname, './links.json'),
      JSON.stringify(links, null, 2),
      'utf-8',
      function(err) {
        if (err) throw err;
      }
  );
}

const symbols = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const generate = (len = 5) => {
  return Array(len)
      .fill(1)
      .reduce(str => str + symbols[Math.floor(Math.random() * symbols.length)], '');
};

function shorten(url) {
  let link = links.data.find(x => x.url === url);
  if (!link) {
    let short = generate();
    while (links.data.find(x => x.short === short)) {
      short = generate();
    }
    link = {
      short: short,
      url: url,
      create: new Date().getTime(),
      redirects: 0
    };
    links.data.push(link);
    save();
  }
  return link;
}

function long(short) {
  return links.data.find(x => x.short === short);
}

module.exports = {
  shorten,
  long
};
