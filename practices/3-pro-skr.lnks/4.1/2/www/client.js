
window.addEventListener('load', main);

function main() {
  document.querySelector('button.shorten-get').addEventListener('click', shortenGet);
}

function shortenGet() {
  const url = document.querySelector('input[name=url]').value;
  fetch(`/shorten?${encodeURIComponent(url)}`)
    .then(res => res.json())
    .then(updateLast)
    .catch(console.error);
}

function updateLast(link) {
  const as = document.querySelector('.res .short a');
  as.textContent = link.short;
  as.setAttribute('href', `/${link.short}`);
  const au = document.querySelector('.res .url a');
  au.textContent = link.url;
  au.setAttribute('href', `${link.url}`);
  document.querySelector('.res .created').textContent = new Date(link.created).toLocaleString();
  document.querySelector('.res .redirects').textContent = link.redirects;
}
