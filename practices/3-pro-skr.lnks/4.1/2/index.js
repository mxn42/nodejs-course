
const express = require('express');
const route = require('./route');

const app = express();
app.use(express.static('www'));
app.get('/shorten', route.getShorten);
app.get('/:short', route.goto);

app.listen(8080, () => {
  console.log('http://localhost:8080/')
});
