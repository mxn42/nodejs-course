
const model = require('./model');

const getShorten = (req, res) => {
  const url = decodeURIComponent(req.query.url);
  const link = model.shorten(url);
  res.json(link);
};

const goto = (req, res) => {
  const short = req.params.short;
  const link = model.long(short);
  if (link) {
    res.redirect(302, link.url);
  }
  else {
    res.status(404).send();
  }
};

module.exports = {
  getShorten,
  goto
};
