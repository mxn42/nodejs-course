
const db = require('../model');

const get = (req, res) => {
  const query = req.originalUrl.substr(req.originalUrl.indexOf('?') + 1);
  const url = decodeURIComponent(query);
  res.json(db.shorten(url));
};

const goto = (req, res) => {
  const short = req.params.short;
  console.log(short);
  const link = db.long(short);
  if (link) {
    res.redirect(301, link.url);
  }
  else {
    res.status(404).send('URL is not found.');
  }
};

module.exports = {
  get,
  goto
};
