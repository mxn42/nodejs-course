
const fs = require('fs');
const dbfile = __dirname + '/../db/links.json';
const db = require(dbfile);
const save = () => fs.writeFile(dbfile, JSON.stringify(db, null, 2), err => err && console.error(err));

const symbols = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const generate = (len = 5) => {
  while (true) {
    let short = [...Array(len)].reduce(a => a + symbols[Math.floor(Math.random() * symbols.length)], '');
    if (!db.data.find(x => x.short === short))
      return short;
  }
};

const shorten = url => {
  let link = db.data.find(x => x.url === url);
  if (!link) {
    link = {
      short: generate(),
      url: url,
      created: new Date().getTime(),
      redirects: 0
    };
    db.data.push(link);
    save();
  }
  return link;
};

const long = short => {
  const link = db.data.find(x => x.short === short);
  if (link) {
    link.redirects += 1;
    save();
  }
  return link;
};

module.exports = {
  shorten,
  long
};
