
const express = require('express');
const bodyParser = require('body-parser');
const api = require('./api');

const app = express();
app.use(express.static('www'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/shorten', api.get);
app.get('/:short', api.goto);

app.listen(8080, () => console.log('http://localhost:8080/'));
