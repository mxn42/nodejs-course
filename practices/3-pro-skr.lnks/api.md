
# REST API LCRUD

- `GET /:short`
- `GET /shorten?url=:url`

- `GET /shorten?:url`
- `POST /shorten`

- `GET /links?count=20&offset=60`
- `GET /links/:short`
- `PATCH /links/:short`
- `DELETE /links/:short`


### Переадресация на полный url

Request
```
GET /:short
```
Response
```
301 Moved Permanently 
:short
```

### Create

Request 1)
```
GET /links?new=:url
```

Request 2) 
```
POST /links

:url
```

Response 
```
201 Created
{
    "short": String,
    "url": :url,
    "created": Number,
    "count": 0 
}
```


### List
Request
```
GET /links?count=20&offset=60
```
Response 
```
200 OK
{
    "count": 20,
    "offset": 60,
    "data": [{
        "short": String,
        "url": :url,
        "created": Number,
        "count": 0 
    }]
}
```


Read
```
GET /links/:short
```
Response 
```
200 OK
{
    "short": :short,
    "url": :url,
    "created": Number,
    "count": Number 
}
```

### Update

PATCH /links/:short
```
PATCH /links/:short

{
    "created": Number,
    "count": Number 
}
```
Response 
```
204 No Content
```

### Delete

```
DELETE /links/:short
```
Response 
```
204 No Content
```
