
# Практика 5: User List

Темы
 - Проблемы CORS в XMLHttpRequest
 - Проблемы credentials в fetch
 - Идемпотентность
 - Проект "Users" -- серверное приложение, REST
 - Использование БД
    - Mongo (native)
    - Mongo (Mogoose)
    - SQL


## Проблемы CORS в XMLHttpRequest

Проблема: [Правило ограничения домена](https://goo.gl/hYm9jQ).

Решение: [CORS (Cross-origin resource sharing)]([https://developer.mozilla.org/ru/docs/Web/HTTP/CORS]) -- позволяет делать запросы между доменами.

`Access-Control-Allow-Origin`

Пример запроса

    GET / HTTP/1.1
    Host: google.com
    Origin: localhost

Пример ответов

    Access-Control-Allow-Origin: http://localhost http://a.site http://b.site

    Access-Control-Allow-Origin: http://localhost 
    Access-Control-Allow-Origin: http://a.site
    Access-Control-Allow-Origin: http://b.site

    Access-Control-Allow-Origin: *

Также обычно треубются отправлят другие заголовки

    Access-Control-Allow-Methods: OPTIONS, GET, POST, PUT, PATCH, DELETE
    Access-Control-Allow-Headers: Origin, Authorization, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept
    Access-Control-Allow-Credentials: true


## Идемпотентность

Идемпотентная операция — действие, многократное повторение которого эквивалентно однократному.

По спецификации GET-запросов в протоколе HTTP: сервер должен возвращать одни и те же ответы на идентичные запросы (при условии, что ресурс не изменился между ними по иным причинам). Такая особенность позволяет кэшировать ответы, снижая нагрузку на сеть.
