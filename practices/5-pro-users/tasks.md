
# Project: User list

## Tasks

### 1. User list

**Model**

    {
      "id": 1,
      "first_name": "Ashley",
      "last_name": "Snyder",
      "email": "asnyder0@booking.com"
    }
        
**API**


Запрос | Результат
--- | ---
`GET  /`                  | домашняя страница
`GET  /users`             | Список всех
`GET  /users/new`         | Форма создания элемента
`POST /users`             | Создание элемента
`GET  /users/:id`         | Чтение одного элемента
`POST /users/:id/update`  | Изменение элемента
`GET  /users/:id/delete`  | Удаление элемента


```javascript
  app.get('/users', (req, res) => {
    const offset  req.query.offset;
    // вернуть список
  });
```

```javascript
  app.post('/users', (req, res) => {
    // создать элемент
	const data = req.body;
  });
```

```javascript
  app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    // вернуть элемент по id
  });
```

```javascript
  app.post('/users/:id/update', (req, res) => {
    const id = req.params.id;
    // обновить элемент по id
	const data = req.body;
  });
```

```javascript
  app.get('/users/:id/delete', (req, res) => {
    const id = req.params.id;
    // удалить элемент по id
  });
```
    

### Data Bases

### 2.1 MongoDB

<https://docs.mongodb.com/manual/crud/#crud>

1. Скачать <http://192.168.19.30:6501/users.json>

2. Импортировать данные из п.1 
   на сервер 192.168.19.30:27017
   в БД /avalon
   в коллекцию /<you-name>

3. Напечатать красиво список пользователей (использовать .padEnd) в формате

```
     | id | Фамилия Имя           | Пол   | Почтовый адрес
```

4. Удалить кривых пользователей

5. Напечатать последних в алфавитном порядке 5 мужчин (или женщин) 

6. Добавить ко всем пользователям свойство domain, который является частью почтового адреса после @. 

7. Для пользователей домена `t.co`, выставить свойство `blocked = true`.

8. Вывесли 10 самых популярных доменов.

### 2.2. MongoDB, Mongoose

9. Создать класс Bird, несколько птичек и сохранить их в Mongo.

### 3. SQL

