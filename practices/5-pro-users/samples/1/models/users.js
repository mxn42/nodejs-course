
const db = require('../db/jsondb');

db.connect(err => {
  if (err) throw err;
  console.log('DB is connected');
});

const list = (req, res) => {
  const offset = Number(req.query.offset) || 0;
  const count = Number(req.query.count) || 10;
  db.select(offset, count, (err, userlist) => {
    if (err) {
      res.statusCode = 500;
      res.send('Печалька');
    }
    else {
      res.render('users', {
        title: 'Users',
        list: userlist,
        offset: offset,
        count: count
      });
    }
  });
};

const newUser = (req, res) => {
  res.render('new_user', {title: 'New user'});
};

const create = (req, res) => {
  console.log(req.body);
  const user_info = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
  };
  db.insert(user_info, (err, user) => {
    if (err) {
      res.statusCode = 500;
      res.send('Что-то пошло не так');
    }
    else {
      res.redirect('/users');
    }
  });
};

const read = (req, res) => {
  res.render('user', req.user);
};

const update = (req, res) => {
  const user_info = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email
  };
  db.update_user(req.user.id, user_info, (err, user) => {
    if (err) {
      res.statusCode = 500;
      res.send('Error');
    }
    else
      res.redirect('/users/' + user.id);
  });
};

const remove = (req, res) => {
  console.log(req.user);
  db.delete_user(req.user.id, err => {
    if (err) {
      res.statusCode = 500;
      res.send('Error');
    }
    else {
      res.redirect('/users');
    }
  });
};

const exists = (req, res, next) => {
  const id = req.params.id;
  db.select_user(id, (err, user) => {
    if (err) {
      res.status(404).send('Not found');
    }
    else {
      if (user) {
        req.user = user;
        next();
      }
      else {
        res.status(404).send('Not found');
      }
    }
  });
};

module.exports = {
  list,
  newUser,
  create,
  read,
  update,
  remove,
  exists
};

