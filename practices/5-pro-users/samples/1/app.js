const express = require('express');
const usersRouter = require('./routes/users');

const app = express();

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }))
   .set('views', './view')
   .set('view engine', 'pug')
   .use(express.static(__dirname + '/www'))
   .get('/', (req, res) => {
     res.render('index', { title: 'Index' });
   })
   .use('/users', usersRouter)
   .listen(6501, () => {
     console.log('Server is running on http://localhost:6501/');
   }).timeout = 30000;
