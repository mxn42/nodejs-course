const fs = require('fs');
const path = require('path');
let db;

const save = callback => {
  fs.writeFile(
    path.join(__dirname, 'users.json'),
    JSON.stringify(db, null, 2),
    callback
  );
};

const connect = (callback) => {
  //db = require('users.json');
  fs.readFile(path.join(__dirname, 'users.json'), 'utf-8', (err, text) => {
    if (err)
      callback(err);
    else {
      db = JSON.parse(text);
      callback(null, db);
    }
  });
};

const select = (offset, count, callback) => {
  if (offset >= db.data.length)
    callback(new RangeError('Offset is invalid'));
  else
    callback(null, db.data.slice(offset, offset + count));
};

const insert = (userinfo, callback) => {
  const user = {
    id: db.meta.next_id++,
    first_name: userinfo.first_name,
    last_name: userinfo.last_name,
    email: userinfo.email
  };
  db.data.push(user);
  save(err => callback(err, user));
};

const select_user = (id, callback) => {
  const user = db.data.find(x => x.id === Number(id));
  if (user)
    callback(null, user);
  else
    callback(new Error('User is not found'));
};

const update_user = (id, userinfo, callback) => {
  const user = db.data.find(x => x.id === Number(id));
  if (!user) {
    callback(new Error('Not found'));
    return;
  }
  user.first_name = userinfo.first_name;
  user.last_name = userinfo.last_name;
  user.email = userinfo.email;
  // Object.assign(user, userinfo);
  save(err => callback(err, user));
};

const delete_user = (id, callback) => {
  const user = db.data.find(x => x.id === Number(id));
  if (!user) {
    callback(new Error('Not found'));
    return;
  }
  db.data.splice(db.data.indexOf(user), 1);
  save(err => callback(err, user));
};

module.exports = {
  connect,
  select,
  insert,
  select_user,
  update_user,
  delete_user
};
