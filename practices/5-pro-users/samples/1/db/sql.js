var sqldriver = require('sqldriver');

var connection;

exports.connect = function(callback) {
  connection = sqldriver.createConnection({
    host: 'host',
    database: 'students',
    user: 'user',
    password: 'password'
  });
  callback(db);
};

exports.select = function(offset, count, callback) {
  connection.query(`
    SELECT * 
    FROM users 
    WHERE LIMIT = ${count} OFFSET=${offset}
  `, function(err, res) {
    if (err) callback(err);
    callback(null, res.rows);
  });
};

exports.insert = function(userinfo, callback) {
  connection.query(`
    INSERT ....
  `, function(err, res) {
    if (err) callback(err);
    callback(null, res.rows);
  });
};

exports.select_user = function(id, callback) {
  connection.query(`
    SELECT * 
    FROM users 
    WHERE id = ${id}
  `, function(err, res) {
    if (err) callback(err);
    callback(null, res.rows[0]);
  });
};

exports.update_user = function(id, userinfo, callback) {
  connection.query(`
    UPDATE ....
    WHERE id = ${id}
  `, function(err, res) {
    if (err) callback(err);
    callback(null, res.rows[0]);
  });
};

exports.delete_user = function(id, callback) {
  connection.query(`
    DELETE ....
    WHERE id = ${id}
  `, function(err, res) {
    if (err) callback(err);
    callback(null, res.rows[0]);
  });
};
