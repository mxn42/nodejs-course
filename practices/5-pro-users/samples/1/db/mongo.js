const MongoClient = require('mongodb').MongoClient;

let users;

const connect = (callback) => {
  MongoClient.connect('mongodb://localhost:27017', (err, client) => {
    if (err) callback(err);
    else {
      const db = client.db('avalon');
      users = db.collection('users');
      callback(null, users);
    }
  });
};

const select = (offset, count, callback) => {
  users.find().skip(offset).limit(count).toArray(callback);
};

const insert = (userInfo, callback) => {
  users.insert(Object.assign({id: users.find().count() + 1}, userInfo), callback);
};

const select_user = (id, callback) => {
  users.findOne({id: Number(id)}, callback);
};

const update_user = (id, patch, callback) => {
  users.updateOne({id: Number(id)}, patch, callback);
};

const delete_user = (id, callback) => {
  users.removeOne({id: Number(id)}, callback);
};

module.exports = {
  connect,
  select,
  insert,
  select_user,
  update_user,
  delete_user
};
