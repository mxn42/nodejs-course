const express = require('express');
const router = express.Router();

const users = require('../models/users');

router.get ('/', users.list);
router.get ('/new', users.newUser);
router.post('/', users.create);

router.all ('/:id', users.exists);
router.all ('/:id/*', users.exists);
router.get ('/:id', users.read);
router.post('/:id/update', users.update);
router.get ('/:id/delete', users.remove);

module.exports = router;
