
const MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://192.168.19.30:27017', (err, client) => {
  if (err) throw err;

  const cats = client.db('cats');

  const home = cats.collection('home');

  home.insert({
    name: 'Bond',
    age: 7
  });

});

//  `${last} ${first}`.padEnd(40, '.')

/*
  CRUD
  - [Read] find, findOne
  - [Create] insert
  - [Update] update, updateOne
  - [Delete] remove, removeOne

*/
  // collection.count
  // collection.find
  // collection.findOne
  // collection.insert
  // collection.remove
  // collection.removeOne
  // collection.update
  // collection.updateOne

