
const MongoClient = require('mongodb').MongoClient;

// http://192.168.19.30:6501/test.js

MongoClient.connect('mongodb://192.168.19.30:27017', (err, client) => {
  if (err) throw err;

  const db = client.db('avalon');

  const collection = db.collection('Denis');

  collection.find().skip(1000).limit(100).toArray((err, data) => {
    for (let user of data) {
      console.log(user.id + ' ' + user.first_name.padEnd(10) + user.last_name.padEnd(10));
    }
  });

  collection.remove({id: 1013});

  client.close();

});

//  `${last} ${first}`.padEnd(40, '.')

/*
  CRUD
  - [Create] insert
  - [Read] find, findOne
  - [Update] update, updateOne
  - [Delete] remove, removeOne

*/
  // collection.count
  // collection.find
  // collection.findOne
  // collection.insert
  // collection.remove
  // collection.removeOne
  // collection.update
  // collection.updateOne

