

<https://docs.mongodb.com/manual/>
<https://docs.mongodb.com/manual/reference/bson-types/>


MongoDB является документо-ориентированной системой, в которой центральным понятием является документ.

mongodb
  - database1
     - collection1
        - document1
        - document2
        - document3
        - document4
     - collection2
        - document5
        - document6
     - collection3
        - document7
        - document8
  - database2
     - collection4
        - document9
        - document10
        - document11



```json
{
    "name": "Bill",
    "surname": "Gates",
    "age": "48",
    "company": {
        "name" : "microsoft",
        "year" : "1974",
        "price" : "300000"
    }
}
```

```javascript
MongoClient.connect('mongodb://localhost:27017/chat', function(err, db) {
  // ...
});
```


## Добавление данных и создание коллекций

Вставка в коллекцию
```text
 db.cats.insert({"name": "Tom", "age": 8, languages: ["english", "kitten"]})
```

## Выборки

```text
 db.users.find({})
 db.users.find({name: 'Tom'})
 db.users.find({name: 'Tom', age: 3})

 SELECT * FROM users WHERE name='Tom' AND age=3


 db.users.find({name: 'Tom'}, {age: 1})
 0 -- не возвращать поле age
 1 -- только age

 db.users.find({name: 'Tom'}, {age: 1, '_id': 0})
 db.users.find({name: 'Tom'}, {age: true, '_id': false})



 db.users.find(function() { return this.name == 'Tom'; })
 db.users.find("this.name == 'Tom'")


```

Вложенные объекты и их свойства
```text
  db.users.insert({"name": "Petr", "age": 28, company: {"name": "microsoft", "country":"USA"}})
  db.users.insert({"name": "Alex", "age": 28, company: {"name": "microsoft", "country":"USA"}})
  db.users.find({"name": "microsoft", "country":"USA"})
```

Регулярные выражения
```text
  db.users.find({name:/T\w+/i})
```

Настройка запросов и сортировка
```text
  db.users.find().limit(3)
  db.users.find().skip(3)
  db.users.find().sort({name: 1})  // по возрастанию (1) или по убыванию (-1)
  db.users.find().sort({name: 1}).skip(3).limit(3)

  db.users.findOne(selector)
  db.users.find(selector).limit(1)
```


## Курсоры

```text
  var cursor = db.users.find(); null;

  while(cursor.hasNext()){
    obj = cursor.next();
    print(obj["name"]);
  }

  cursor.forEach(function(obj){
    print(obj.name);
  })
```
Чтобы получить курсор и сразу же не выводить все содержащиеся в нем данные, после метода find() добавляет через точку с запятой выражение null;


## Операторы

```text
  db.users.count()
  db.users.find({name: "Tom"}).skip(2).count(true)
```

Операторы
- $gt (больше чем)                   >
- $lt (меньше чем)                   <
- $gte (больше или равно)            >=
- $lte (меньше или равно)            <=
- $ne (не соответствующие)           !=
- $in (в массиве)
- $or (или)
- $size (length)
- $exists (наличие/отсутствие)
- $regex (регулярное выражение)
```text
  db.users.find ({age: {$lt : 30}})
  db.users.find ({age: {$gt : 30}})
  db.users.find ({age: {$gt : 30, $lt: 50}})
  db.users.find ({age: {$ne : 22}})

  db.cats.find ({name: 'Tom', age: 1})



  db.users.find ({age: {$in : [22, 32]}})

  db.users.find ({$or : [{name: "Tom"}, {age: 22}]})
  db.users.find ({name: "Tom", $or : [{age: 22}, {languages: "german"}]})
  db.users.find ({languages: {$size:2}})

  db.users.find ({company: {$exists:true}})

  db.users.find ({name: {$regex:"b"}})
```

## Обновление

```text
  db.users.save({name: "Eugene", age : 29, languages: ["english", "german", "spanish"]})
```

## Удаление

```text
  db.users.remove({name : "Tom"})

  db.users.remove({name : /T\w+/i})
  db.users.remove({age: {$lt : 30}})
  db.users.remove({name : "Tom"}, true)
  db.users.remove({})

  db.users.drop()

  db.dropDatabase()
```


## Использование

MongoDB Node.JS Driver <br>
<https://github.com/mongodb/node-mongodb-native>
```shell
$ npm install mongodb
```
