
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/chat');

var catScheme = mongoose.Schema({
  name: String
});

catScheme.methods.say = function() {
  console.log('%s: %s', this.name, 'Meow');
};

var Cat = mongoose.model('Cat', catScheme);

var tom = new Cat({name: 'Tom'});

tom.save(function(err) {
  tom.say();
});

var leo = new Cat({name: 'Leo'});

leo.save(function(err) {
  leo.say();
});