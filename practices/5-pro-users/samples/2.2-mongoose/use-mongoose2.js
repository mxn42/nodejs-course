
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/chat');

var catScheme = mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true
  },
  age: {
    type: Number,
    required: false
  },
  born: {
    type: Date,
    required: false
  }
});

catScheme.methods.say = function() {
  console.log('%s: %s', this.name, 'Meow');
};

var Cat = mongoose.model('Cat', catScheme);

var tom = new Cat({name: 'Tom'});
var leo = new Cat({name: 'Leo'});

tom.save(function(err, data) {
  if (err) throw err;
  tom.say(data);
});

leo.save()
   .then(function(data) {
      leo.say(data);
   })
   .catch(function(err) {
      throw err;
   });
