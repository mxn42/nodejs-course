
var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://localhost:27017/chat', function(err, db) {
  if (err) throw err;
  console.log("Connected to server");

  var cursor = db.collection('cats').find({name: 'Tom'});

  cursor.toArray(function(err, data) {
    if (err) throw err;
    console.log(data);
    db.close();
  });

  var garries = db.collection('cats').find({name: 'Garry'});

  console.log('1');
  garries.forEach(function(cat) {
    console.log(cat);
  });

  db.collection('cats').remove({name: 'Garry'});

  console.log('2');
  garries.forEach(function(cat) {
    console.log(cat);
  });



});
