const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/avalon');

const Bird = mongoose.model('Bird', {
  name: String,
  color: String,
  age: Number
});

const redBird = new Bird({
  name: 'Petr',
  color: 'red',
  age: 10
});


redBird.name = 'Green';

redBird.save();
