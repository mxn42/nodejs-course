const express = require('express');
const config = require('./config');

const app = express();
app.use(express.static('www'));

const http = require('http').Server(app);
const io = require('socket.io')(http);

require('./chat')(io);

// Start service
http.listen(config.port, () => {
  console.log(`Listening http://localhost:${config.port}/`);
});
app.timeout = config.responseTimeout;
