const auth = require('./auth');

module.exports = io => {

  let clientId = 1000;
  io.on('connection', socket => {

    socket.id = clientId++;
    console.log('id:', socket.id);

    socket.on('login', ({name, password}) => {
      socket.user = auth.user(name, password);
      if (socket.user) {
        console.log(socket.user.name);
        socket.emit('auth', true);
      }
      else
        socket.emit('auth', false);
    });

    socket.on('say', text => {
      if (socket.user) {
        const message = {
          from: socket.user.name,
          text: text,
          timestamp: Date.now()
        };
        console.log(`${socket.user.name}: ${text}`);
        socket.broadcast.emit('listen', message);
      }
    });
  });
};
