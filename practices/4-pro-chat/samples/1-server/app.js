const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');

const config = require('./config');
const log = require('./middleware/log');
const base = require('./routes/base');
const auth = require('./routes/auth');
const chat = require('./routes/chat');

const app = express();
app.set('views', './views');
app.set('view engine', 'pug');

// Middlewares
app.use(log);
app.use(session(config.session));
app.use(bodyParser.urlencoded({extended: true}));

// Routes
app.get('/', base.root);
app.get('/login', auth.loginForm);
app.post('/login', auth.login);
app.get('/logoff', auth.logoff);
app.use(auth.check);
app.use('/chat', chat);

// Start service
app.listen(config.port, () => {
  console.log(`Listen http://localhost:${config.port}/`);
});
app.timeout = config.responseTimeout;
