const users = require('../../../data/users');

const hashFn = (password, salt) => {
  return `${salt}%%%${password}`;
};

const findUser = (name, password) => {
  const user = users.find(x => x.name === name);
  if (user && user.hash === hashFn(password, user.salt))
    return user;
  else
    return null;
};

module.exports = {
  user: findUser
};
