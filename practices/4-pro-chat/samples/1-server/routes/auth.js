
const auth = require('../model/auth');

const loginForm = (req, res) => {
  res.render('login', {title: 'Login'});
};

const login = (req, res) => {
  const user = auth.user(req.body.Name, req.body.Password);
  if (user) {
    req.session.user = user;
    req.session.timestamp = Date.now();
    res.redirect('/');
  }
  else
    res.status(403).send('Invalid authorization.');
};

const logoff = (req, res) => {
  delete req.session.user;
  res.redirect('/');
};

const check = (req, res, next) => {
  if (!req.session.user)
    res.redirect('/');
  else
    next();
};

module.exports = {
  loginForm,
  login,
  logoff,
  check,
};
