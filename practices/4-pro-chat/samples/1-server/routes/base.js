
const root = (req, res) => {
  res.render('index', {
    title: '',
    user: req.session.user? req.session.user.name : ''
  });
};


module.exports = {
  root
};
