const express = require('express');
const router = express.Router();
const messages = require('../model/messages');

router.get('/', (req, res) => {
  res.render('chat', {
    title: 'Chat'
  });
});

router.get('/post-form', (req, res) => {
  res.render('post-form', {
    name: req.session.user.name
  });
});

router.get('/messages', (req, res) => {
  const timestamp = req.query.since || 0; // req.session.timestamp || Date.now() || 0;
  const showMessages = messages.since(timestamp);
  res.render('messages', {
    since: timestamp,
    messages: showMessages.map(x => Object.assign({}, {
      name: x.from,
      date: new Date(x.timestamp).toLocaleDateString(),
      text: x.text
    }))
  });
});

router.post('/messages', (req, res) => {
  messages.add({
    user: req.session.user.name,
    text: req.body.Text
  });
  res.redirect('/chat/post-form');
});

module.exports = router;
