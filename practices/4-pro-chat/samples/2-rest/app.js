const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');

const config = require('./config');
const log = require('./middleware/log');
const options = require('./middleware/options');
const auth = require('./routes/auth');
const chat = require('./routes/chat');

const app = express();

// Middlewares
app.use(require('./middleware/log'));
app.use(require('./middleware/cors'));
app.use(require('./middleware/options'));
app.use(session(config.session));
app.use(bodyParser.json());

// Routes
app.use(express.static('www'));
app.post('/login', auth.login);
app.get('/logoff', auth.logoff);
app.use(auth.check);
app.use('/chat', chat);

// Start service
app.listen(config.port, () => {
  console.log(`Listen http://localhost:${config.port}/`);
});
app.timeout = config.responseTimeout;
