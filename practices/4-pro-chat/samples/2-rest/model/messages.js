const fs = require('fs');
const dbfile = '../../../data/messages.json';
const data = require(dbfile);
const save = () => fs.writeFile(`${__dirname}/${dbfile}`, JSON.stringify(data, null, 2), 'utf-8', console.error);

const since = (timestamp = Data.now()) => {
  return data.filter(x => x.timestamp > timestamp);
};

const add = ({user, text}) => {
  const message = {
    timestamp: Date.now(),
    from: user,
    text,
  };
  data.push(message);
  save();
  return message;
};

module.exports = {
  since,
  add
};
