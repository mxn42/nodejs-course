module.exports = (req, res, next) => {
  if (req.method === 'OPTIONS')
    res.end();
  else
    next();
};
