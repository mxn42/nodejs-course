const express = require('express');
const router = express.Router();
const messages = require('../model/messages');

router.get('/messages', (req, res) => {
  const timestamp = req.query.since || 0; // req.session.timestamp || Date.now() || 0;
  res.json(messages.since(timestamp));
});

router.post('/messages', (req, res) => {
  console.log(req.session.user.name, ': ', req.body.text);
  const message = messages.add({
    user: req.session.user.name,
    text: req.body.text
  });
  res.json(message);
});

module.exports = router;
