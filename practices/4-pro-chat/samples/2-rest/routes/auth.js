
const auth = require('../model/auth');

const login = (req, res) => {
  const user = auth.user(req.body.name, req.body.password);
  if (user) {
    req.session.user = user;
    req.session.timestamp = Date.now();
    res.json(true);
  }
  else
    res.json(false);
};

const logoff = (req, res) => {
  delete req.session.user;
  res.status(204).send();
};

const check = (req, res, next) => {
  if (!req.session.user)
    res.redirect('/');
  else
    next();
};

module.exports = {
  login,
  logoff,
  check,
};
