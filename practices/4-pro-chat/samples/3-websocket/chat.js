
const auth = require('./model/auth');

module.exports = io => {

  io.on('connection', socket => {
    console.log('user connected');

    socket.on('disconnect', () => {
      console.log('user disconnected');
    });

    socket.on('login', data => {
      console.log('user login');
      const user = auth.user(data.name, data.password);
      if (user) {
        socket.user = user;
        socket.emit('auth', true);
      }
      else
        socket.emit('auth', false);
    });

    socket.on('logoff', () => {
      console.log('user logoff');
      delete socket.user;
    });

    socket.on('client-message', text => {
      if (socket.user) {
        console.log('user message');
        io.emit('server-message', {
          from: socket.user.name,
          timestamp: Date.now(),
          text
        });
      }
    });
  });

};
