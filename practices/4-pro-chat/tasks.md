
# JavaScript Chat

## Tasks

### 1. Серверный чат на основе html-форм, url-encoded

**1.1.** Автоматическое перенаправление с корневого адреса `/`.

    GET /
   
**1.2.** Авторизация

  Форма логин-пароль
   
    GET /login

  Запрос авторизации

    POST /login
    
    Name=James%20Bond&Password=jsBD%261962%25

  Сброс авторизации
  
    GET /logoff

**1.3.** Отображение чата
   
    GET /chat

**1.3.** Список сообщений
   
    GET /chat/messages?since=1523189951107

**1.4.** Форма добавления поста в список

    GET /chat/post-form

**1.5.** Добавление поста в список

    POST /chat/messages
    
    Text=My%20name%20is%20Bond.%20James%20Bond


### 2. Чат на основе Single-Page-App, Ajax, JSON

**2.1** Добавление статических файлов для SPA

    GET /

На странице
 - форма логина или кнопка выхода
 - список сообщений
 - форма добавления сообщения 

**2.2.** API

  Запрос авторизации

    POST /login

    {
        "name": "James Bond",
        "password": "jsBD&1962%"
    }

  Сброс авторизации
  
    GET /logoff

  Список сообщений

    GET /chat/messages?since=1523189951107

    [{  
      "from": "Dude",
      "text": "What's up, bro?",
      "timestamp": 1523188230846
    }, { 
      "from": "Guy",
      "text": "Yo, bro!",
      "timestamp": 1523188351174
    }]

  Добавление сообщения
  
    POST /chat/messages

    "My name is Bond. James Bond"
  

### 3. Чат на основе WebSockets, JSON

  Подключение
  
    server.io.on('connection')

  Запрос авторизации

    client.socket.emit('login', {
      name: "James Bond",
      password: "jsBD&1962%"
    });
    server.socket.on('login', function(auth) { ... })
    
  Получение подтверждение авторизации №1 (серверное событие)
  
    server.socket.emit('auth', true);
    client.socket.on('auth', function(ok) { ... })
  
  Получение подтверждение авторизации №2 (параметр .emit(..., ack))
  
    client.socket.emit('login', {
      name: "James Bond",
      password: "jsBD&1962%"
    }, function(ok) { ... });
    server.socket.on('login', function(auth, ack) { 
        ...
        ack(true); 
    })

  Сброс авторизации
  
    client.socket.emit('logoff');
    server.socket.on('logoff', function() { ... })

  Список сообщений: не нужен
  
  Отправка от клиента серверу
  
    client.socket.emit('client-message', 'My name is Bond. James Bond');
    server.socket.on('client-message', function(message) { ... })

  Отправка от сервера клиенту (публичное, включая адресата)
    
    server.io.emit('server-message', {
      from: 'James Bond',
      timestamp: 1523211548688,
      text: 'My name is Bond. James Bond'
    });

  Отправка от сервера клиенту (публичное, не включая адресата)
    
    server.socket.broadcast.emit('server-message', {
      from: 'James Bond',
      timestamp: 1523211548688,
      text: 'My name is Bond. James Bond'
    });

  Отправка от сервера клиенту (персональное)
  
    server.socket.emit('server-message', {
      from: 'James Bond',
      timestamp: 1523211548688,
      text: 'My name is Bond. James Bond'
    });

  Отправка от сервера клиенту (получение клиентом)

    client.socket.on('server-message', function(message) { ... });
  
