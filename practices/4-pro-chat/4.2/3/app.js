const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(express.static('www'));

const users = {};

io.on('connection', (socket) => {

  socket.on('login', ({name, password}) => {
    if (name && !users[name]) {
      users[name] = {name};
      socket.user = users[name];
      socket.emit('auth', true);
    }
    else
      socket.emit('auth', false);
  });

  socket.on('say', text => {
    if (socket.user)
      io.emit('listen', {
        from: socket.user.name,
        timestamp: Date.now(),
        text
      });
  });

  setInterval(() => {
    const randomNumber = Math.floor(Math.random() * 1000);
    socket.emit('kuku', randomNumber);
  }, 1000);

});

http.listen(9444, () => {
  console.log('listen http://192.168.19.30:9444/');
});
