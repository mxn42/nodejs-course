
const fs = require('fs');

fs.readFile('task19.csv', 'utf-8', (err, data) => {

  if (err) {
    console.error(err);
    return;
  }

  const users = data.trim().split(/\r*\n/g).map(line => {
    const fields = line.split(',');
    return {
      name: fields[0],
      region: fields[1],
      math: fields[2],
      phy: fields[3]
    }
  });
  console.log(users);

  fs.writeFile('users.json', JSON.stringify(users, null, 2) , 'utf-8', err => {
    if (err) throw err;
  });

});
