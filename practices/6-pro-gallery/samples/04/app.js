
const express = require('express');
const bodyParser = require('body-parser');
const publisher = require('./publisher');
const app = express();

app.use(bodyParser.json());
app.set('views', './views');
app.set('view engine', 'pug');

app.post('/:topic/event', (req, res) => {
  const message = req.body;
  publisher.publish(req.param.topic, message);
  req.send('Ok');
});

app.get('/:topic/event'...)

app.get('/:topic/subscribe'...)

app.get('/:topic/subscribe/confirm'...)

app.get('/:topic/unsubscribe'...)

app.get('/:topic/unsubscribe/confirm'...)

app.listen();
