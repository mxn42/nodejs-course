
POST /:topic/event

    {
        from: String,
        subject: String,
        text: Text
    }

GET /:topic/listeners

    [{
       email: String,
       data: Date
    },
    {
       email: String,
       data: Date
    }]

/views/message.pug

GET /:topic/subscribe?m=email

/views/subscribe.pug

GET /:topic/subscribe/confirm?m=email&token=@#$@$@#$@#$@#$@$#@

/views/subscribe-success.pug

GET /:topic/unsubscribe?m=email

/views/unsubscribe.pug

GET /:topic/unsubscribe/confirm?m=email&token=@#$@$@#$@#$@#$@$#@

/views/unsubscribe-success.pug





