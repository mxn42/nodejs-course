const sendmail = require('sendmail')();

function sendMessage(email, from, subject, text) {
  sendmail({
    from: from,
    to: email,
    subject: subject,
    text: text,
  });
}

const listeners = [];

module.exports = {
  publish: function(topic, {from, subject, text}) {
    const topicListeners = listeners.filter(x => x.topic === topic);
    for (let listener of topicListeners)
      sendMessage(listener.email, from, subject, TEMPLATE_TEXT(text));
  },

  subscibe: function(topic, email) {
    const confirmationCode = rand();
    sendMessage(email, from, subject, SUBSCRIBE_TEMPLATE(confirmationCode));
  },

  confirmSubscibe: function(topic, email) {
    listeners.push({topic, email});
  },

  unsubscibe: function(topic, email) {
    const confirmationCode = rand();
    sendMessage(email, from, subject, UNSUBSCRIBE_TEMPLATE(confirmationCode));
  },

  confirmUnSubscibe: function(topic, email) {
    const removeList = listeners.filter(x => x.topic === topic && x.email === email);
    for (let li of removeList)
      listeners.splice(listeners.indexOf(li), 1);
  }
};
