const express = require('express');
const multer = require('multer');

const app = express();
const upload = multer({ destination: 'uploads/' });

app.set('views', './view');
app.set('view engine', 'pug');
app.use(express.static(__dirname + '/www'));

app.get('/', (req, res) => {
  res.render('index', { title: 'Index' });
});
app.post('/upload', upload.single('avatar'), function(req, res) {
  console.dir(req.files);
  res.send(`Uploaded ${req.files.length} files`);
});

app.listen(6501, () => {
  console.log('Server is running on http://localhost:6501/');
});
app.timeout = 30000;
