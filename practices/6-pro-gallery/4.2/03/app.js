const express = require("express");
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

const topics = {};
function sendEmail(to, message) {
  // sendmail
  sendmail({
    from: message.from,
    to: to.email,
    subject: message.topic,
    html: message.text,
  }, function(err, reply) {
    console.log(err && err.stack);
    console.dir(reply);
  });
}

app.post('/:topic/message', (req, res) => {
  const topic = req.param.topic;
  const {from, text} = req.body;

  if (topics[topic]) {
    const message = {topic, from, text, date: Date.now()};
    for (let listener of topics[topic].listeners)
      sendEmail(listener, MESSAGE_TEMPLATE(message));
  }
  res.send('Отправлено');
});

app.get('/:topic/listeners', (req, res) => {
  if (topics[topic])
    res.json(topics[topic].listeners);
  else {
    res.json();
  }
});

app.get('/:topic/subsribe', (req, res) => {
  const topic = req.param.topic;
  const email = req.query.email;
  const token = generate();
  sendEmail(email, SUBCRIBE_TEMPLATE(message));
  res.send("Подтверждение отправлено");
});

app.get('/:topic/subsribe-confirm', (req, res) => {
  const topic = req.param.topic;
  const email = req.query.email;
  const token = req.query.token;
  if (!topics[topic])
    topics[topic] = { listeners: []};
  topics[topic].push({
    email,
    date: Date.now()
  });
  res.send("Вы подписаны");
});

app.get('/:topic/subsribe', (req, res) => {

app.get('/:topic/subsribe-confirm'


app.listen(8080);
