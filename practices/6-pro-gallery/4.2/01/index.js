
const fs = require('fs');

fs.readFile('./task19.csv', 'utf-8',  function(err, data) {
  if (err) {
    console.error(err);
    return;
  }

  const lines = data.trim().split(/\r*\n/);
  const headers = lines.shift().split(',');
  console.log(headers);

  const users = lines.map(function(line) {
    const fields = line.split(',');
    const obj = {};
    for (let i = 0; i < fields.length; i += 1)
      obj[headers[i]] = fields[i];
    return obj;
  });

  //console.log(users);

  fs.writeFile('users.json', JSON.stringify(users, null, 2), 'utf-8', function(err) {
    if (err) {
      console.error(err);
      return;
    }
    console.log('Все хорошо');
  });


});
