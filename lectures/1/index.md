
# Lecture 1: WT Node.sj, modules, file system

  - Введение: исторический обзор, JavaScript, V8, Node.js
  - Установка
  - Основы работы с Node.js
    - Программа на Node.js: объявление, запуск
    - Передача параметров приложению
    - Модули, работа с модулями
    - NPM, установка сторонних модулей
    - Асинхронность в Node.js
    - Работа с файлами
    - События
  - Ресурсы для изучения

[Practice 1](../../practices/1)

## Введение: исторический обзор, JavaScript, V8, Node.js

> Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient. Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
  https://nodejs.org

Node.js -- среда выполнения JavaScript, предоставляющая: файловые операции, систему модулей, хранилище модулей. Node.js спроектирована для реализации веб-серверов. 

   * Node.js создан Ryan Dahl в 2009 году
   * Официальный сайт: <https://nodejs.org>
   * JavaScript движок: Chrome's V8 JavaScript engine

JavaScript был предназначен для выполнения в браузере для создания интерактивных страниц, язык получился изящным. До Node.js уже существовали платформы для выполнения JavaScript вне браузера: Rhino (Unix), JSC (MacOS), WSH (Windows). Однако, они каждый из них имел существенные недостатки и ни один из них не позицировался как самодостаточная платформа.

Node.js расширяет JavaScript на использование в файловой системе, предоставляет быстрой способ создания веб-страниц и имеем большой и удобный репозиторий кода. Что позволяет использоваться JavaScript не только для программирования веб-страниц, но и для любых других целей. 

На текущий момент Node.js используется прежде всего в двух областях: веб-приложения и автоматизация.
 
Ключевые идеи
  - Эффективность в своей области
  - Простота
  - Событийно ориентированная разработка
  - Большое количество соединений, масшабируемость
  - Открытый код
  - Сообщество, быстрый развитие 

**Node.js = JS * V8 * I/O * NPM**

  * JavaScript — превосходный язык, огромная база кода, огромное сообщество, широчайшее применение
  * V8 — движок JS с открытым кодом от Google
    - Быстрый
    - Экономный 
    - Поддерживает новые версии JavaScript
    - Широкие возможностями отладки и профилирования
  * I/O — файловые и сетевые библиотеки
  * NPM — менеджер пакетов ("все что нужно уже кем-то написано"), много специализированных библиотек для разработки
    - Основные: https://nodejs.org/dist/latest-v6.x/docs/api/
    - Файловые операции
    - Веб-сервера с поддержкой множества соединений и одновременных запросов
    - Подключения к базам данных
    - Шаблоны для решений, быстрое прототипирование
    - Широкие возможности для разработки
      - Фреймворки для тестирования
      - Фреймворки для автоматизации
      - Фреймворки для реализации интерфейсов (API)

Note 1. Можно упомянуть про общий код клиента и сервера, но здесь есть большие ограничения: задачи на клиенте и сервере разные и код редко будет подходящим.

Note 2. При выполенении JavaScript под Node.js нет браузера, а значит нет DOM, нет объекта document. 

## Установка

<https://nodejs.org>. Поскольку JavaScript активно развивается, настоятельно рекомендуется использовать последнюю версию Node.js. 
 
Документация <https://nodejs.org/en/docs/>, уровни стабильности <https://nodejs.org/en/docs/api/documentation.html> отражают стабильность (неизменность) API.

Проверка установки
```
> node -v
v9.3.0
> node
| console.log('Hello, World!')
Hello, World!
> node hello.js
Hello, World!
```
 
## Подключение модулей

Модули — отсутствующая идея в родном JavaScript. Пусть у нас есть два файла JavaScript. Как указать, что один файл использует другой файл? В контексте браузера файлы подключаются тегами script, но они подключаются к странице, а не друг к другу.

Для решения этой проблемы было придумано несколько решений. Одна из них (requirejs) воплощена в Node.js. Пусть у нас есть файл app.js. Мы хотим выделить часть функциональности в отдельный файл для удобства разработки (отладка, тестирование, сопровождение). Чтобы подключить один файл в другой делаем функцией require.

Отличие от подключения модуля в браузере и в Node.js: замыкание и объекты.
 
Виды модулей <https://nodejs.org/en/docs/api/modules.html>

  - JS — файл JavaScript
  - NODE — скомпилированный файл C
  - JSON — файл JSON, позволяющий подключать данные, включая конфиги
  - <directory> — папка с кодом модулями, загружаемый файл index.js

Установка модулей, `node_modules`, иерархический метод поиска модуля <https://nodejs.org/dist/latest-v6.x/docs/api/modules.html#modules_all_together>

Кэширование модулей.

Параметры вызова модуля.

## package.json 

## Работа с NPM: создание пакета, получение пакетов

	https://registry.npmjs.org

	npm init
	
	package.json
	
	npm adduser
	
	npm publish
	npm unpublish
	
	npm help
	
	npm search
	
	npm install module-name
	npm remove module-name

	node_modules
	
	npm install
	npm update
 
## Работа с файлами: чтение, запись, свойства

<https://nodejs.org/api/fs.html>
    - readFile, сравнение с readFileSync
    - writeFile
    - readdir

## Ресурсы для изучения
 - [Node.js v9.5.0 Documentation](https://nodejs.org/dist/latest-v9.x/docs/api/)
 - [Руководство по Node.js](https://metanit.com/web/nodejs/)
 - [Скринкаст NODE.JS](https://learn.javascript.ru/screencast/nodejs)
 - [FRONTENDER MAGAZINE: Искусство Node](https://frontender.info/art-of-node/)
 - [Node.js для начинающих](http://nodebeginner.ru/) 
