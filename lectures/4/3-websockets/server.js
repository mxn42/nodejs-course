
var express = require('express');
var app = express();
app.use(express.static(__dirname + '/public'));

var server = require('http').createServer(app);
var io = require('socket.io')(server);
server.listen(3000, function() {
  console.log('Server is running on http://localhost:3000/');
});

var countConnection = 0;
var id = 1;

io.on('connection', function(socket) {
  countConnection += 1;
  socket.name = 'Client ' + id++;
  console.log('%s is connected.', socket.name);
  io.emit('change-user', {
    countConnection: countConnection,
    user: socket.name,
    type: 'Connect'
  });

  socket.on('disconnect', function() {
    countConnection -= 1;
    console.log('Client disconnected.');
    io.emit('change-user', {
      countConnection: countConnection,
      user: socket.name,
      type: 'Leave'
    });
  });

  socket.on('client-message', function(text) {
    console.log("%s: %s", socket.name, text);
    io.emit('server-message', {
      name: socket.name,
      text: text
    });
  })


  //callbacks
});


