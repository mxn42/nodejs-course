
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/chat');

var Cat = mongoose.model('Cat', {
  name: String
}); // cats

var bond = new Cat({name: 'Bond', age: 5});

bond.save(function(err) {
  if (err) throw err;
  console.log('Bond is saved');
});

bond.save().then(function(doc) {
  console.log('%s is saved', doc.name);
});

// не сохранит поле
var leo = new Cat({name: 'Leo', age: 5});
console.log(leo);

leo.save(function(err, leo, affected) {

});
