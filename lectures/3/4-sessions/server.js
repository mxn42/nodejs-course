
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const config = require('./config.json');
const app = express();

app.set('view engine', 'pug');
app.use(bodyParser.urlencoded({extended: true}));
app.use(session(config.session));

const port = config.port;
app.listen(port, () => console.log('Listen on http://localhost:' + port));

app.use((req, res, next) => {
  console.log(req.session.NOVEMBER);
  if (req.session.NOVEMBER)
    req.session.NOVEMBER.tutbyl += 1;
  else {
    req.session.NOVEMBER = {
      since: new Date(),
      tutbyl: 1
    }
  }
  next();
});

app.get('/', (req, res) => {
  if (req.session.NOVEMBER.name) {
    res.render('index', {
      title: 'Private',
      PROFILE: req.session.NOVEMBER
    })
  }
  else {
    res.redirect('/login');
    // res.render('login', {title: 'Login'});
  }
});

app.get('/login', (req, res) => {
  res.render('login', {title: 'Login'});
});

app.post('/login', (req, res) => {
  // create session
  console.log(req.body.name, req.body.password);
  // login-password validation
  if (verify(req.body.name, req.body.password)) {
    req.session.NOVEMBER.name = req.body.name;
  }
  res.redirect('/');
});

function verify(name, password) {
  return config.users[name] && config.users[name] == password;
}

// function verify(name, password) {
//   if (config.users[name] &&
//       config.users[name].encodedPassword ==
//       SALTHASH(password, config.users[name].salt))
//     return true;
//   else
//     return false;
// }

app.get('/logout', (req, res) => {
  delete req.session.NOVEMBER.name;
  res.redirect('/');
});

