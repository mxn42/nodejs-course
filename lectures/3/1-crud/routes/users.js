var express = require('express');
var router = express.Router();

var users = require('../models/users');

router.get ('/', users.list);
router.get ('/new', users.new);
router.post('/', users.create);

router.all ('/:id', users.exists);
router.all ('/:id/*', users.exists);
router.get ('/:id', users.read);
router.post('/:id/update', users.update);
router.get ('/:id/delete', users.delete);

module.exports = router;
