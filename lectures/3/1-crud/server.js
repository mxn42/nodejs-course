
var express = require('express');
var app = express();
app.listen(6500, function () { console.log('Server is running on http://localhost:6500/') });

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/public'));

app.set('views', './view');
app.set('view engine', 'jade');

var indexRouter = require('./routes/index');
app.use('/', indexRouter);

var usersRouter = require('./routes/users');
app.use('/users', usersRouter);
