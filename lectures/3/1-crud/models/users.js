
var db = require('../db');

db.connect(function(err) {
  if (err)
    throw err;
  console.log('DB is connected');
});

exports.list = function(req, res) {
  var offset = Number(req.query.offset) || 0;
  var count = Number(req.query.count) || 10;
  db.select(offset, count, function(err, userlist) {
    if (err) {
      res.statusCode = 400;
      res.send('Печалька');
    }
    else {
      res.render('users', {
        title: 'Users',
        list: userlist,
        offset: offset,
        count: count
      });
    }
  });
};

exports.new = function(req, res) {
  res.render('new_user', {title: 'New user'});
};

exports.create = function(req, res) {
  console.log(req.body);
  var user_info = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email
  };
  db.insert(user_info, function(err, user) {
    if (err) {
      res.statusCode = 500;
      res.send('Что-то пошло не так');
    }
    else {
      res.redirect('/users');
    }
  });
};

exports.read = function(req, res) {
  res.render('user', req.user);
};

exports.update = function(req, res) {
  var user_info = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email
  };
  db.update_user(req.user.id, user_info, function(err, user) {
    if (err) {
      res.statusCode = 500;
      res.send('Error');
    }
    else
      res.redirect('/users/' + user.id);
  });
};

exports.delete = function(req, res) {
  console.log(req.user);
  db.delete_user(req.user.id, function(err) {
    if (err) {
      res.statusCode = 500;
      res.send('Error');
    }
    else {
      res.redirect('/users');
    }
  });
};

exports.exists = function(req, res, next) {
  var id = req.params.id;
  db.select_user(id, function(err, user) {
    if (err) {
      res.statusCode = 404;
      res.send('Not found');
    }
    else {
      req.user = user;
      next();
    }
  });
};
