var fs = require('fs');
var path = require('path');

var db;

exports.connect = function(callback) {
  //db = require('users.json');
  fs.readFile(path.join(__dirname, 'users.json'), 'utf-8', function(err, text) {
    if (err)
      callback(err);
    else {
      db = JSON.parse(text);
      callback(null);
    }
  });
};

exports.select = function(offset, count, callback) {
  if (offset >= db.data.length)
    callback(new RangeError('Offset is invalid'));
  else
    callback(null, db.data.slice(offset, offset + count));
};

exports.insert = function(userinfo, callback) {
  var user = {
    id: db.meta.next_id++,
    first_name: userinfo.first_name,
    last_name: userinfo.last_name,
    email: userinfo.email,
    gender: ''
  };
  db.data.push(user);
  fs.writeFile(
      path.join(__dirname, 'users.json'),
      JSON.stringify(db, null, 2),
      function(err) {
        if (err)
          callback(err);
        else
          callback(null, user);
      }
  );
};

exports.select_user = function(id, callback) {
  var user = db.data.find(function(user) {
    return user.id == id;
  });
  if (user)
    callback(null, user);
  else
    callback(new Error('User is not found'));
};

exports.update_user = function(id, userinfo, callback) {
  var user = db.data.find(function(user) {
    return user.id == id;
  });
  if (!user) {
    callback(new Error('Not found'));
    return;
  }
  user.first_name = userinfo.first_name;
  user.last_name = userinfo.last_name;
  user.email = userinfo.email;
  fs.writeFile(
      path.join(__dirname, 'users.json'),
      JSON.stringify(db, null, 2),
      function(err) {
        if (err)
          callback(err);
        else
          callback(null, user);
      }
  );
};

exports.delete_user = function(id, callback) {
  var user = db.data.find(function(user) {
    return user.id == id;
  });
  if (!user) {
    callback(new Error('Not found'));
    return;
  }
  db.data.splice(db.data.indexOf(user), 1);
  fs.writeFile(
      path.join(__dirname, 'users.json'),
      JSON.stringify(db, null, 2),
      function(err) {
        if (err)
          callback(err);
        else
          callback(null, user);
      }
  );
};
