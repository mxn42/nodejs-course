
const express = require('express');
const config = require('./config.json');
const app = express();

app.set('view engine', 'pug');

const port = config.port;
app.listen(port, () => console.log('Listen on http://localhost:' + port));

app.get('/', (req, res) => {
  res.render('index', {title: 'Index'});
});

app.get('/internal', (req, res) => {
  res.render('internal', {title: 'Internal'});
});
