
const http = require('http');
const server = http.createServer();

const fs = require('fs');

server.on('request', (req, res) => {

  if (req.method === 'GET' && req.url == '/lorem1')
    fs.readFile('lorem1.txt', 'utf-8', (err, data) => {
      if (err) {
        console.error(err);
        res.statusCode = 400;
        res.end(`Oops: ${err.message}`);
      }
      res.end(data);
    });
  else if (req.method === 'GET' && req.url == '/lorem2')
    fs.readFile('lorem2.txt', 'utf-8', (err, data) => {
      if (err) {
        console.error(err);
        res.statusCode = 400;
        res.end(`Oops: ${err.message}`);
      }
      res.end(data);
    });
  else {
    res.statusCode = 404;
    res.end('NOT FOUND');
  }

});

server.listen(5555, err => {
  if (err) throw err;
  console.log('Ok');
});
