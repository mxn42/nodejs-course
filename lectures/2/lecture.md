
# Express, async, patterns

  - Express
    - Simplification web development
    - Static files
    - Routing
    - Middleware
      - Next
    - Redirect
  - **Templating**
  - Patterns: "Publish–Subscribe", "MVC"

**Practices**

  - Работа с фреймворком Express.
  - Запуск Express-приложений.
  - Применение Middleware
    - изменение заголовков HTTP

**Resources**

- <http://expressjs.com/ru/>
- <https://nodeguide.ru/doc/modules-you-should-know/express/>


## Intro
  - [Цена сложности](https://habrahabr.ru/company/express42/blog/185488/)

## Express

  - [Express](http://expressjs.com/ru/)
  
  гибкая система маршрутизации запросов
  перенаправления
  динамические представления
  уточнение контента

  особое внимание производительности
  обработка представлений и поддержка частичных шаблонов
  поддержка конфигураций на основе окружений
  оповещения, интегрированные с сессиями
  максимальное покрытие тестами
  утилиты для быстрой генерации остова приложений
  настройки представлений на уровне приложений

  поддержка сессий
  кэш API
  поддержка mime
  поддержка ETag
  постоянные оповещения
  поддержка кук
  JSON RPC
  логирование  
  
**Installation**

> npm install -g express
> express -s
> npm install
> npm start

    - public/
    - routes/
    - views/
    - package.json
    - readme.md
    - index.js
    - app.js

**Steps**

  - Hello, world
  - Send
  - Middleware (промежуточные обработчики) <http://expressjs.com/ru/guide/writing-middleware.html>
    - next() 
    - routing
    - error handling <http://expressjs.com/ru/guide/error-handling.html>
    - update headers (CORS, Cache)
    - templates <http://expressjs.com/ru/guide/using-template-engines.html>
  - Plugins
    - bodyParser
    - session
    - logger
    - cookieParser
    - errorHandler
      


## Методы ответа
  <http://expressjs.com/ru/guide/routing.html>

  Методы в объекте ответа (res), перечисленные в таблице ниже, могут передавать ответ клиенту и завершать цикл “запрос-ответ”. Если ни один из этих методов не будет вызван из обработчика маршрута, клиентский запрос зависнет.
  
  |Метод	|Описание|
  |---|---|
  |res.download()	| Приглашение загрузки файла.
  |res.end()	| Завершение процесса ответа.
  |res.json()	| Отправка ответа JSON.
  |res.jsonp()	| Отправка ответа JSON с поддержкой JSONP.
  |res.redirect()	| Перенаправление ответа.
  |res.render()	| Вывод шаблона представления.
  |res.send()	| Отправка ответа различных типов.
  |res.sendFile	| Отправка файла в виде потока октетов.
  |res.sendStatus()	| Установка кода состояния ответа и отправка представления в виде строки в качестве тела ответа.
    
## app.route()  
  app.route('/book')
    .get(function(req, res) {
      res.send('Get a random book');
    })
    .post(function(req, res) {
      res.send('Add a book');
    })
    .put(function(req, res) {
      res.send('Update the book');
    });
    
    
## express.Router    
  var router = express.Router();  
    
