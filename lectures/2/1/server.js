
const http = require('http');

const server = http.createServer();

server.on('request', function(request, response) {
  console.log(`METHOD "${request.method}", URL: "${request.url}"`);
  response.end('Hello, world!');
});

// localhost:5574
server.listen(5574);
console.log('server started');
