
const http = require('http');
const server = http.createServer();

server.on('request', (request, response) => {
  console.log(`${request.method} ${request.url}`);
});

server.on('request', (request, response) => {
  const now = new Date();
  response.end(now.toLocaleString());
});

server.listen(5555, err => console.log('Server is stated in :5555'));
