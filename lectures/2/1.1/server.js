
const http = require('http');

const server = http.createServer();

server.on('request', function(request, response) {
  console.log(`METHOD "${request.method}", URL: "${request.url}"`);
  response.end(`${request.method} ${request.url}`);
});

// localhost:5574
server.listen(5555);
console.log('server started');
