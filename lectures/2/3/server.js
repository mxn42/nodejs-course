
const http = require('http');
const server = http.createServer();

const fs = require('fs');

server.on('request', (req, res) => {

  fs.readFile('lorem.txt', 'utf-8', (err, data) => {
    if (err) {
      console.error(err);
      res.statusCode = 400;
      res.end(`Oops: ${err.message}`);
    }
    res.end(data);
  });

});

server.listen(5555, err => {
  if (err) throw err;
  console.log('Ok');
});
