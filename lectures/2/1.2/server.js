
const http = require('http');

const server = http.createServer();

server.on('request', function(request, response) {
  console.log(`METHOD "${request.method}", URL: "${request.url}"`);
  response.end(`${request.method} ${request.url}`);
});

server.listen(5555, function(err) {
  if (err) throw err;
  console.log('listen on :5555');
});
