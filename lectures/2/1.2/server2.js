
require('http').createServer()
  .on('request', (request, response) => {
    console.log(`METHOD "${request.method}", URL: "${request.url}"`);
    response.end(`${request.method} ${request.url}`);
  })
  .listen(5555, err => {
    if (err) throw err;
    console.log('listen on :5555');
  });
