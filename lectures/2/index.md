
# Lecture 2: Express web applications

  - Клиент-серверная разработка: два подхода php и node
  
  - Веб-сервер на Node.js
    - Получение запросов и отправка ответов
    - Асинхронность
    - Роутинг (маршрутизация)
    - HTTP, REST

  - Express
    - Обработка запроса
    - Роутинг (маршрутизация)
    - Статические файлы
    - Middleware

  - Etc    
    - Url-encoded, XML, JSON
    - Работа с формами
    - Шаблоны (templates)
    - "Publish–Subscribe", "MVC"


 - HTTP: Internet vs WWW
 - Web-Server
 - 1) Hello server
 - 2) Echo server
 - 3) Time server
 - 4) File server
 - Async interaction
 - Static files
 - Express
   - application, install
   - routing: hello, echo, time
   - static files 


**Practice 2**
 - Создание веб-сервера на Express
 - Простейшие веб-сервисы
 - Статичный сайт
 - Использование шаблонов
 - Middleware
