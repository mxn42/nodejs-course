
# Node.js

<https://bitbucket.org/mxn42/course-node>

### [Lecture 1: Intro, modules, file system](./1/index.md)

  - Исторический обзор: JavaScript, V8, Node.js
  - Ресурсы для изучения
  - Основы работы с Node.js
    - Программа на Node.js: объявление, запуск
    - Передача параметров приложению
    - Модули, работа с модулями
    - NPM, установка сторонних модулей
    - Асинхронность в Node.js
    - Работа с файлами
    - События

**Practice 1**
 - Первое приложение, запуск с параметрами
 - Вычисления
 - Прочитать и записать файлы
 - Модули, package.json, npm
 - Просмотреть все файлы в папке
 - Запросить картинки с веб-сайта и сохранить

### [Lecture 2: Express web applications](./2/index.md)

  - Клиент-серверная разработка: два подхода php и node
  - Веб-сервер на Node.js
  - Express
    - Обработка запроса
    - Роутинг (маршрутизация)
    - Статические файлы
  - Middleware
  - Url-encoded, XML, JSON
  - Работа с формами
  - Шаблоны

**Practice 2**
 - Создание веб-сервера на Express
 - Простейшие веб-сервисы
 - Статичный сайт
 - Использование шаблонов
 - Middleware

### [Lecture 3: Web server, REST](./3/index.md)

  - Типовые задачи веб-сервера
    - Получение запросов и отправка ответов
    - Асинхронность
    - Роутинг (маршрутизация)
    - Статические файлы
    - Шаблоны
    - Middleware
  - REST
    - CRUD
    - POST-запросы и отправка форм
    - JSON и AJAX

**Practice 3**
 - Создание веб-приложений на Express
 - REST
 - Роутинг, организация файлов

### [Lecture 4: DB](./4/index.md)

  - CRUD SQL
    - Запрос данных
    - Условия, группировка
    - Добавление
    - Изменение
    - Удаление 
    - Применение CRUD в PostgreSQL
  - CRUD DocumentDB
    - Применение CRUD в MondgoDB
    - Применение CRUD в MondgoDB+Mongouse
  - Использование DB: создание и удаление баз, роли и права

**Practice 4**
 - Использование DB
 
**Practice 5**
 - Web-Sockets
 
**Practice 6**
 - Подготовка к зачету


### [Lecture X: WebSockets, http client, patterns](./X/index.md)

  - WebSockets, SocketIO
  - CORS
  - http клиент
  - Принципы проектирования: KISS, Less is more, DRY, YAGNI, TMTOWTDI
  - Шаблоны проектирования, шаблон "Издатель-Подписчик" (Observer)
  - "Реактивность"
  - Антипаттерны
  - Разработка через страдание

### [Lecture XX: Development](./XX/index.md)

  - Разработка на Node.js
    - Nodemon
    - Тестирование: Assert, Jasmine, Jest
    - Профилирование

  - Тонкости Node.js
    - Объект global и глобальные переменные
    - Stream
    - Stream и отправка файлов
    - Pipe
    - веб-сервер без Express

  - Express 
    - Тестирование Express

  - Лицензии
