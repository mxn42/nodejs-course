const links = require('./data/links');

const express = require('express');
const app = express();

app.use(function(req, res, next) {
  console.log(req.method, req.url);
  next();
});
app.use(express.static('www'));

app.get('/:short', () => {});
app.get('/links/new', () => {});
app.post('/links', () => {});
app.get('/links', () => {});

app.use('/links/:short', (req, res, next) => {
  const short = req.param.short;
  const link = links.data.find(x => x.short === short);

  /*
  let foundLink = null;
  for (var i = 0; i < links.data.length; i += 1) {
    var link = links.data[i];
    if (link.short === short) {
      foundLink = link;
      break;
    }
  }

  let foundLink = null;
  links.data.forEach(function(link) {
    if (link.short === short) {
      foundLink = link;
    }
  });

  let foundLink = null;
  let filtered = links.data.filter(function(link) {
    return (link.short === short);
  });
  if (filtered.length > 0)
    foundLink = filtered[0];
  */

  if (link) {
    req.link = link;
    next();
  } else {
    res.status(404).send('Link is not found.')
  }
});

app.get('/links/:short', (req, res) => {
  res.json(req.link);
});

app.patch('/links/:short', () => {
  // for (var key in req.body) {
  //   req.link[key] = req.body[key];
  // }
  Object.assign(req.link, req.body);
  res.status(204).send();
});

app.delete('/links/:short', (req, res) => {
  links.data.splice(links.indexOf(req.link), 1);
  res.status(204).send();
});

app.listen(5544, () => console.log('http://localhost:5544'));
