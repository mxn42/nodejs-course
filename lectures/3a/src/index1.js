const links = require('./data/links');

const express = require('express');
const app = express();

app.use(function(req, res, next) {
  console.log(req.method, req.url);
  next();
});
app.use(express.static('www'));

app.get('/:short', (req, res) => {
  const short = req.param.short;
  res.redirect(short);
});
const create = (req, res) => {
  let short;
  while (short = Math.floor(Math.random() * 10000000000))
    if (!links.data.find(x => x.short === short)) break;
  const link = {
    short: short,
    url: req.body,
    created: new Date().getTime(),
    count: 0
  };
  links.data.push(link);
  res.json(link);
};
app.get('/links/new', create);
app.post('/links', create);
app.get('/links', (req, res) => {
  const count = req.query.count || 20;
  const offset = req.query.offset || 0;
  const sub = links.data.slice(offset, count);
  res.json(sub);
});

app.use('/links/:short', (req, res, next) => {
  const short = req.param.short;
  const link = links.data.find(x => x.short === short);
  if (link) {
    req.link = link;
    next();
  } else {
    res.status(404).send('Link is not found.')
  }
});

app.get('/links/:short', (req, res) => {
  res.json(req.link);
});

app.patch('/links/:short', () => {
  Object.assign(req.link, req.body);
  res.status(204).send();
});

app.delete('/links/:short', (req, res) => {
  links.data.splice(links.indexOf(req.link), 1);
  res.status(204).send();
});

app.listen(5544, () => console.log('http://localhost:5544'));
