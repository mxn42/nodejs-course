try {
  var db = connection('mongodb...');
  var item = db.find(1111);
  item.a = 5;
  db.save(item);
}
catch(err) {
  console.error(err);
}

// CALLBACKS
connection('mongodb...', done1, fail1);

function done1(db) {
  db.find(1111, done2, fail2);
}
function fail1() {}

function done2(db, item) {
  item.a = 5;
  db.save(item, done3, fail3);
}
function fail2() {}

function done3() {
  console.log('Saved!');
}
function fail3() {}

// NODEJS


// PROMISES
connection('mongodb...')
    .then(function(db) {
      return db.find(111);
    })
    .then(function(item) {
      item.a = 5;
      return item.save();
    })
    .then(function() {
      console.log('Saved!');
    })
    .catch(function(err) {
      console.error();
    });

try {
  var db = connection('mongodb...');
  var item = db.find(1111);
  item.a = 5;
  db.save(item);
}
catch(err) {
  console.error(err);
}